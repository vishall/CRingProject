% Copyright 2016 Liber Mathematicae, licensed under GNU FDL v1.3
% main author: 
%   Markus Pflaum 
%
\section{Rational numbers}
\para 
Even though the integers form an abelian group with respect to addition, multiplicative inverses of integers $n \ne 1,-1$ do not 
exist in $\Z$. The argument is as follows. First observe that $0$ is not 
multiplicatively invertible in $\Z$ and even not in any extension ring $R$ 
of $\Z$ because if it were with inverse $m$,  then $1 = 0 \cdot m = 0$ and 
$r = 1 \cdot r = 0 \cdot r = 0$ for all $r\in R$. But this contradicts that 
$R$ is assumed to be an extension ring of $\Z$, which, to remind the reader, 
is a ring $R$ in which $\Z$ is embedded by an injective ring homomorphism 
$\Z \hookrightarrow R$. To verify that also every integer $n \ne 0,1,-1 $ does
not have a multiplicative inverse in $\Z$, assume 
that $m \in \Z$ is one, i.e.~that $n\cdot m = m \cdot n = 1$. If $n >0$, 
then $m >0$ as well, since otherwise 
$n\cdot (-m) = -1 <0$ and $ n\cdot (-m) >0$ by monotony of multiplication
which is a contradiction. But then $m \geq 1$ and $n > 1$, hence 
$n \cdot m >1$ which contradicts the assumption that $m$ is a multiplicative inverse of $n$.
If $n<0$, then $-m$ is a multiplicative inverse of $-n$ which is strictly positive and which we already have ruled out to have a multiplicative inverse. 
So the elements of $\Z \setminus \{ 1,-1\}$ are all not 
invertible in $\Z$. 

The ring (or better field as we will later see) of rational numbers $\Q$ 
will be defined as the minimal extension of $\Z$ in which all non-zero 
integers are multiplicatively invertible. The 
construction of $\Q$ is via \emph{localization} by the set of non-zero integers
meaning by forming abstract quotients, called fractions, of integers
by non-zero ones. The process  resembles the construction of the 
Grothendieck group, but it is not the same since we do not want to invert 
the zero element of $\Z$. 
The symbol $\Q$ for the field of rational numbers goes 
back to Giuseppe Peano who introduced it 1895  after \emph{quoziente}, the 
Italian word for \emph{quotient}.

\subsec{Localization}

\begin{definition}
  A subset $S $ of a commutative ring $R$ is called \emph{multiplicative}
  if it contains $1$ and if for all $r,s \in S$ the product $rs$ 
  is in $S$ again. 
\end{definition}

Let $R$ be a commutative ring, and $S\subset R$ a multiplicative subset. 
On the cartesian product $R\times S$ we introduce an equivalence relation
as follows.  Two pairs $(p,q),(r,s)\in R\times S$ are called equivalent,
in signs $(p,q) \sim (r,s)$, if $p s t = r q  t$ 
for some $t\in S$.  Obviously, $\sim$ is reflexive and symmetric. 
To verify transitivity, assume that $(a,b) \sim (p,q)$ and $(p,q) \sim (r,s)$.
Choose $d,t \in  S$ such that $a q  d = p b  d$ and 
$p s  t = r  q  t$. Then 
\[
  a  s (q  d  t) =  a q d (s t) = pbd  (st) = 
  p s t ( b  d  ) = rqt  (bd) 
  =  r  b  ( q  d t )\ ,
\]
hence $ (a,b) \sim (r,s)$, so $\sim$ is transitive and an equivalence relation 
indeed. We denote the equivalence class of $(p,q)$ by 
\[
   \frac pq 
\]
and call it the \emph{abstract quotient} or \emph{fraction} of $p$ by $q$. 
The set of fractions $ \frac pq$ with $p\in R$, $q\in S$ is denoted by 
$S^{-1}R$ and called the \emph{localization} of $R$ by $S$. 

\begin{lemma}
  For every element $t$ of a multiplicative subset $S$ of a commutative ring $R$ 
  and every element $\frac pq \in S^{-1}R$ the fractions 
   $\frac pq$ and $\frac{pt}{qt}$ coincide. 
\end{lemma}

\begin{proof}
  This is clear since $p\, qt = pt\, q$.
\end{proof}

\begin{proposition}
\label{thm:localization-ring-multiplicative-subset}
  Let $R$ be a commutative ring and $S\subset R$ a multiplicative subset. 
  \begin{romanlist}
  \item\label{ite:localization-ring}
  The localization $S^{-1} R$ carries a ring structure with addition
  \[
    + : S^{-1} R \times S^{-1} R \to S^{-1} R, \quad 
   \Big( \frac p q , \frac r s \Big) \mapsto \frac{ps + rq}{qs} \ ,
  \]
  multiplication 
   \[
    \cdot : S^{-1} R \times S^{-1} R \to S^{-1} R, \quad 
    \Big( \frac p q , \frac r s \Big) \mapsto \frac{pq}{qs} \ ,
  \]
  zero element $0 := \frac 0 1$ and multiplicative identity $1 := \frac 11$. 
  \item 
   For every $s\in S$, the fraction $\frac 1s$ is the inverse of $\frac s1$ in $S^{-1} R$. 
  \item
   The map $R \to S^{-1} R$, $r \mapsto \frac r1$ is a ring homomorphism. 
  \item
  In case $S$ does not contain any zero divisors that is if $s r \neq 0$ for all $s\in S$ and $r\in R_{\ne 0}$,  
  then the canonical ring homomorphism $R \to S^{-1} R$ is injective. 
  Moreover in this case, two fractions $\frac pq$ and 
  $\frac rs$ are identical if and only if $ps = rq$. 
  \end{romanlist}
\end{proposition}

\begin{proof}
  \begin{adromanlist}
  \item  
  First one needs to verify that addition and multiplication are well-defined. 
  To this end let $\frac pq = \frac{p'}{q'}$ and  $\frac rs = \frac{r'}{s'}$.
  This means there are $t,u \in S$ such that 
  $p  q' t = p' q t$ and $r s' u = r' s u$. Then 
  \[
    (ps + rq) \, q' s' \, t u = pq't (s s' u) + r s' u (q q' t) =
    p'qt (ss' u) + r's u( qq't) =   (p's' + r'q') \, q s \, t u  \ ,
  \]
  so the sum of two fractions is well-defined. Next 
  \[
    pr \, q's' \, ut = (pq't)(rs'u) = (p' q t)( r' s u) = 
    p'r' \, qs \, ut \ ,
  \]
  so the product of two fractions is well-defined as well. 

  To prove associativity of addition compute
  \begin{equation*}
  \begin{split}
    \Big( \frac a b +  \frac p q \Big) + \frac r s & = 
    \frac{aq + pb}{bq}  + \frac r s =
    \frac{(aq + pb)s + rbq}{bqs} = \\
    & = \frac{a qs + (ps+rq)b}{bqs} = 
    \frac a b + \frac{ps + rq}{qs} = 
    \frac a b + \Big( \frac p q + \frac r s \Big) \ .  
  \end{split}
  \end{equation*}
  Since $R$ is a commutative ring, 
  \[
    \frac p q + \frac r s  = \frac{ps + rq}{qs} =
    \frac{rq + ps}{sq}  =  \frac r s  + \frac p q \ ,
  \]
  hence addition in $S^{-1}R$ is commutative. 
  The fraction $\frac 01$ acts as neutral element by addition:
  \[
   \frac 01 + \frac pq =  \frac{0 \cdot q + p \cdot 1}{1 \cdot q} = \frac pq \ .
  \] 
  And the additive inverse of $\frac p q$ is given by the fraction 
  $\frac{-p}{q}$\ :
  \[
   \frac p q + \frac{-p}{q} = \frac{p q + (-p)q}{q^2} = \frac{(p -p)q}{q^2}
   = \frac{0}{q^2} = \frac{0}{1} \ .
  \]
  So we have shown  that $S^{-1}R$ with addition is an abelian group. 
  Let us consider multiplication now. Multiplication in $S^{-1}R$ is obviously 
  associative since it is in $R$. More precisely, 
  \begin{equation*}
  \begin{split}
    \Big( \frac a b  \cdot  \frac p q \Big) \cdot \frac r s & = 
    \frac{ap}{bq} \cdot \frac r s = \frac{(ap)r}{(bq)s} = 
     \frac{a(pr)}{b(qs)} =  \frac ab \cdot \frac{pr}{qs} = 
    \frac a b \cdot \Big( \frac p q\cdot \frac r s \Big) \ .  
  \end{split}
  \end{equation*} 
  Similarly, multiplication in $S^{-1}R$ is commutative:
  \[
     \frac p q \cdot \frac r s = \frac{pr}{qs} = \frac{rp}{sq} = 
     \frac r s \cdot \frac p q \ .
  \]
  The element $\frac 11$ acts neutrally by multiplication, since
  \[
    \frac 11 \cdot  \frac p q = \frac{1 \cdot p}{1 \cdot q} = \frac pq \ .
  \]   
  Note that $\frac 11 = \frac tt$ for all $t\in S$ since 
  $1 \cdot t = t \cdot 1$. Using this, 
  one proves that  multiplication distributes over addition:
  \begin{equation*}
  \begin{split}
    \frac a b \cdot  \Big( \frac p q + \frac r s \Big) & =
    \frac ab \cdot  \frac{ps + rq}{qs}  = 
    \frac{a (ps + rq)}{bqs} = \frac bb \cdot \frac{a (ps + rq)}{bqs} = \\
    & = \frac{aps + arq}{bqs} = \frac{ap \, bs + ar \, bq}{bq \, bs} 
    = \frac{ap}{bq} + \frac{ar}{bs} 
    =  \frac a b \cdot   \frac p q +  \frac a b \cdot \frac r s \ .
  \end{split}
  \end{equation*} 
  We have verified that $S^{-1}R$ is a commutative ring. 
  \item
  Since for every $s \in S$ the relation 
  \[
    \lambda (s) \cdot \frac 1s = \frac s1 \cdot \frac 1s = \frac ss = 1
  \]
  holds true, $\frac 1s  $ is the inverse of $\lambda (s)$. 
  \item
  The canonical mapping 
  $\lambda : R \to S^{-1}R$, $r \mapsto \frac r1$ is a ring 
  homomorphism, since
  \[
    \frac r1 + \frac s1 = \frac{r + s}{1}, \quad  
    \frac r1 \cdot \frac s1 = \frac{rs}{1}, 
  \]
  and since $\frac 01$ and $\frac11$ are  neutral  with respect 
  to addition and multiplication, respectively. 
  \item
  Now we show that $\lambda$ is injective 
  if $S$ does not contain any zero divisors. Assume that 
  $\lambda(r)=\lambda (s)$. Then $\lambda (r -s)=0$, hence there exists 
  a $t\in S$ such that  $(r-s) t = 0$. But since $S$ does not have any zero 
  divisors $r=s$, so $\lambda$ is injective. 

  Finally consider the equation
  $\frac pq = \frac rs$. It is equivalent to $pst = rqt$ for some $t\in S$,
  hence to  $(ps-rq)t=0$. If $S$ does not have zero divisors,  the latter is 
  equivalent to $ps = rq$.  
  This finishes the proof of the proposition. 
  \end{adromanlist}
\end{proof}

\begin{remark}
  In the case where the multiplicative subset $S$ of the commutative ring $R$ 
  does not contain any zero divisors, one identifies $R$ with its image in 
  $S^{-1} R$ and denotes every element in $S^{-1} R$ of the form $\frac r1$ 
  just by $r$.
\end{remark}


\subsec{The ordered field of rational numbers $\Q$}
%
By Proposition \ref{thm:ring-integers-no-nonzero-zero-divisors}, the set $ S := \nzZ$ of non-zero integers
is multiplicative. Hence the following definition makes sense.
\begin{definition}
 The \emph{field of rational numbers} $\Q$ is defined as the localization 
 $S^{-1}\Q$ of $\Z$ by the multiplicative set $S $ of non-zero integers. 
\end{definition}



\begin{thmanddef}\label{thm:rationals-field}
  The set $\Q$ together with addition $+$ and multiplication $\cdot$ as
  binary operations and the elements $0 := \frac 01$ and $1:= \frac 11$ as neutral elements 
  is a \emph{field} which means that the following axioms hold true:
  \begin{axiomlist}[Fld]
  \item \label{axiom:field-rationals-addition-abelian-monoid}
         $\Q$ together with addition $+$ and the element $0$ is an abelian group.  
  \item \label{axiom:field-rationals-multiplication-abelian-monoid-nonzero-elements-invertible} 
         $\Q$ together with multiplication $\cdot$ and the element $1$ is an abelian monoid such that 
         every element $\nzQ := \Q \setminus \{ 0 \} $ has  a multiplicative inverse.  
  \item \label{axiom:field-rationals-distributivity}
         Multiplication distributes from the left and the right over addition. 
  \item\label{axiom:field-rationals-nonequality-neutral-elements} The neutral elements $0$ and $1$ are not equal. 
  \end{axiomlist}


  Moreover, the canonical map $\Z \hookrightarrow \Q$, 
  $p \mapsto n := \frac p1$ is an injective  ring homomorphism, so $\Z$ can be identified with 
  its image $\big\{ \frac n1 \in \Q \mid n \in \Z \}$ in $\Q$. 
\end{thmanddef}

\begin{proof}
  By \Cref{thm:localization-ring-multiplicative-subset} we know that $\Q$ is a commutative ring and 
  that $\Z \hookrightarrow \Q$, $p \to \frac p1$ an injective ring homomorphism.  
  In particular this verifies Axioms \ref{axiom:field-rationals-addition-abelian-monoid} and 
  \ref{axiom:field-rationals-distributivity} and that $\Q$ with multiplication $\cdot$ and the element $1$ is
  an abelian monoid. 
  Since $0 \neq 1$ in $\Z$ (because $\N \hookrightarrow \Z$ is injective, $1 =s(0)$ and 
  $0$ is not in the image of the successor map $s : \N \to \N$), Axiom \ref{axiom:field-rationals-nonequality-neutral-elements} 
  holds true. It remains to show that every non-zero element of $\Q$ has a multiplicative inverse. 
  So let $\frac pq \in \Q$ be non-zero. Then $p$ and $q$ are both in $\nzZ$. The element
  $\frac qp$ now is the multiplicative inverse of $\frac pq$. This finishes the proof. 
\end{proof}

\begin{definition}
  A set $\fldF$ equipped with binary operations $+$ and $\cdot$ on $\fldF$ and two elements $0,1\in \fldF$ 
  is called a \emph{field} if Axioms \ref{axiom:field-rationals-addition-abelian-monoid}  to \ref{axiom:field-rationals-nonequality-neutral-elements}
  above are satisfied (after replacing $\Q$ by $\fldF$). If in addition $\leq$ is a total order relation on 
  $\fldF$ such that the monotony axioms below are satisfied as well, then $\fldF$, or more precisley
  $(\fldF, +,\cdot,0,1,\leq)$ is called an \emph{ordered field}: 
  \begin{axiomlist}[M]
  \item
  \label{axiom:monotony-addition-ordered-field} \textup{Monotony of addition}\\
   For all $a,b\in \fldF$ and $c\in \fldF$ the relation $a < b$ implies 
   $a + c < b + c$.  
  \item
   \label{axiom:monotony-multiplication-ordered-field}  \textup{Monotony of multiplication} \\
   For all  $a,b\in \fldF$ and $c\in \fldF_{> 0} := \{x \in \fldF \mid x > 0\} $ the relation $a < b$ implies $a \cdot c < b \cdot c$.
  \end{axiomlist}
\end{definition}

\begin{definition}
  A rational number $\frac pq \in \Q$ is called \emph{less or equal} than a rational number $\frac rs \in \Q$, in signs $\frac pq \leq \frac rs$,
  if the integer $ (rq - ps) qs $ lies in $\N$.  
\end{definition}

\begin{theorem}
  The field $\Q$ together with the binary relation $\leq$ is an ordered field. Moreover, the canonical embedding
  $\Z \hookrightarrow \Q$, $p \mapsto \frac p1$ is order preserving which means that $p < q$ for $p,q\in \Z$ implies
  $\frac p1 < \frac q1$. 
\end{theorem}
\begin{proof}
  Obviously, $\leq$ is reflexive, since for $\frac pq \in \Q$ the integer $ (pq - pq) q^2 $ vanishes. 

  If $\frac pq \leq \frac rs $ and $\frac rs \leq \frac pq$, then both $ (rq - ps) qs $ and its additive inverse
  $ (ps - rq) qs $ are elements of $\N$, hence $ (rq - ps) qs =0$ which implies that 
  $\frac rs - \frac pq = \frac{rq - ps}{qs} = \frac{ (rq - ps)qs}{(qs)^2} = 0$. So $\leq $ is antisymmetric. 

  If $\frac ab \leq \frac pq$ and $\frac pq   \leq \frac rs$, then 
  $ (pb - aq) bq \geq 0 $ and $ (rq -ps ) qs \geq 0 $. Hence 
  $ \big( (rb - as )bs \big) qq = ( rbq - aqs )bqs = (rq -ps ) qs b^2 + (pb - aq) bq  s^2 > 0$, since
  $b^2 > 0$ and $s^2>0$. Since $q^2 >0$, $(rb - as )bs$ follows by monotony of multiplication in $\Z$. 
  This proves that $\leq$  is transitive. So we have shown that $\leq$ is an order relation on $\Q$.
  

  Since for two rational numbers $\frac pq$ and $\frac rs$ the integer  $ (rq -ps ) qs$ is either positive, zero, or negative, 
  the trichotomy law holds true, and $\leq$ is a total order. 

  Note that the relation $\frac rs > 0 $ is equivalent to $ rs >0$. Hence, if $\frac ab < \frac pq$ and $\frac rs >0$, 
  then $\frac{ar}{bs} < \frac{pr}{qs}$ since $(prbs - arqs)bsqs = \big( (pb-aq)bq\big) (rs) s^2  > 0$. Therefore, monotony of multiplication holds.
  Now let $\frac rs \in \Q$ be arbitrary and $\frac ab < \frac pq$ as before. 
  Then 
  \[
     \frac ab + \frac rs = \frac{as + rb}{sb}\quad \text{and} \quad  \frac pq + \frac rs = \frac{ps + rq}{sq} \ .
  \]
  Now compute
  \[
    \big( (ps + rq) sb - (as + rb) (sq) \big) sbsq = \big( (pb -aq) bq \big) s^4 > 0 \ .
  \]
  Hence $\frac ab + \frac rs < \frac pq + \frac rs$, and monotony of addition is finally proved as well.  

  Finally assume $p < q$ for integers $p,q$. Then $(q\cdot 1 - p \cdot 1) \cdot 1^2 = p - q > 0$,
  hence $\frac p1 \leq \frac q1$. But $p \neq q$ since the ring homomorphism $\Z\hookrightarrow \Q$ is 
  injective. So one even has  $\frac p1 < \frac q1$ as claimed.
\end{proof}

\begin{theorem}\label{thm:field-rational-numbers-archimedean-ordered}
  The field of rational numbers $\Q$ is an archimedean ordered field that is for every pair 
  of rational numbers $\frac pq$, $\frac rs$ with $\frac rs > 0$ there exists a natural number 
  $n \in \N$ such that $\frac pq < n \, \frac rs $. 
\end{theorem}

\begin{proof}
  By \Cref{thm:properties-order-ordered-integral-domain} \ref{ite:inverse-preserving-positivity}
  and since $\frac rs > 0$, the claim is equivalent to the existence of a natural number 
  $n \in \N$ such that $\frac{ps}{qr} < n$. So it suffices to prove that for every rational 
  $\frac pq$ there exists  $n \in \N$ such that $ \frac pq < n$. Let us show this. 
 .After possibly multiplying both $p$ and $q$ by $-1$ we can assume that $ q \in \gzN$. If 
  $p \leq 0$, put $n=1$ and observe $ p \leq 0 < q = n \cdot q$. If $p>0$, then $p \in \N$, so  
  by the archimedean property for natural numbers \ref{thm:archimedean-property-natural-numbers} 
  there exists $n \in \N$ such that $ p < n \cdot q $. Now observe that $0 < \frac 1q$ because 
  $(1 \cdot 1 - 0 \cdot q) \cdot 1 \cdot q = q > 0$. 
  By monotony of multiplication, we can now conclude   
  from $ p < n \cdot q $ that $\frac pq < n$.  This proves the claim. 
\end{proof}

\subsec{Ordered fields}
\para
In the following we introduce new concepts for ordered fields and will derive properties which hold in 
any ordered field, so in particular in $\Q$.  

Let us recall some notation and assume that $(\fldF,+,\cdot,0,1, \leq)$ is an ordered field.
Then $\fldF$ (or better $(\fldF,+,\cdot,0,1, \leq)$) is in particular an ordered integral domain, so
the sets  $\fldF_{>0}$ of \emph{positive} elements and $\fldF_{<0}$ of \emph{negative} elements are 
defined as $\{ x \in \fldF \mid 0 < x \}$ and  $\{ x \in \fldF \mid x < 0 \}$, respectively, cf.~\Cref{def:ordered-commutative-ring}. The sets $\fldF_{\geq 0} := \{ x \in \fldF \mid 0 \leq x \}$ and 
$\fldF_{\leq 0} := \{ x \in \fldF \mid x \leq 0 \}$ are the the sets of \emph{non-negative}
and \emph{non-positive} elements of  $\fldF$, respectively. 

\begin{propanddef}
  Let $(\fldF,+,\cdot,0,1, \leq)$ be an ordered field . Then the set $\fldF^+:= \fldF_{\geq 0}$  of  
  non-negative elements is a \emph{positive cone} that is it satisfies the following axioms:
  \begin{axiomlist}[PC]
  \item\label{ite:positive-cone-stability-addition}\hspace{2mm}
    For all $x,y \in \fldF^+$ the sum $x +y$ lies in $ \fldF^+$.
  \item\label{ite:positive-cone-stability-multiplication}\hspace{2mm}
    For all $x,y \in \fldF^+$ the product $x \cdot y$ lies in $\fldF^+$.
  \item\label{ite:positive-cone-containing-squares}\hspace{2mm} 
    The square $x^2$ is an element of $\fldF^+$ for every $x\in \fldF$. 
  \item\label{ite:positive-cone-not-containing-negative-one}\hspace{2mm}
    The element $-1$ does not lie in $\fldF^+$.
  \item\label{ite:union-positive-cone-negative-cone}\hspace{2mm} 
    The field $\fldF$ is the union of $ \fldF^+$ and $\fldF^- := -\fldF^+$. 
  \end{axiomlist}
\end{propanddef}

\begin{proof}
    \ref{ite:positive-cone-stability-addition} follows by monotony of addition and transitivity:
    $x + y \geq x \geq 0$,  
    \ref{ite:positive-cone-stability-multiplication} by monotony of multiplication:
    $ x \cdot y \geq x \cdot 0 = 0$. 
    \ref{ite:positive-cone-containing-squares} is a consequence of 
    \Cref{thm:properties-order-ordered-integral-domain} \ref{ite:square-positive}. 
    Adding $-1$ to the inequality 
    $0 < 1$ gives $ -1 < 0$ which entails \ref{ite:positive-cone-not-containing-negative-one}. 
    Next observe that by \Cref{thm:properties-order-ordered-integral-domain} \ref{ite:antimonotony-negative}
    $\fldF^- = \fldF_{\leq 0}$. \ref{ite:union-positive-cone-negative-cone} then follows by the trichotomy law
    for $\leq$. 
\end{proof}

\begin{remark}
 The proof of the proposition also shows $\fldF^- = \fldF_{\leq 0}$. 
\end{remark}

\begin{proposition}
  Let $(\fldF,+,\cdot,0,1,\leq)$ be an ordered field. Then the map 
  \[
    \Z \to \fldF, \quad p \mapsto p \cdot 1 := 
    \begin{cases}
       \sum\limits_{i=1}^p 1 & \text{if } p \in \gzZ,\\
       0 &  \text{if } p = 0, \\
       - \sum\limits_{i=1}^{-p} 1  & \text{if } p \in  \lzZ,
    \end{cases}
  \] 
  is an order preserving embedding of rings.  
\end{proposition}

\begin{proof}
  By Proposition \ref{thm:power-operation-group-homomorphism}, the map $\Z \to \fldF$, $p \mapsto p \cdot 1$
  is a group homomorphism with respect to the additive group structures on $\Z$ and $\fldF$. 
  
  Now let $p,q \in \Z$ and assume $q < p$. Then $n = p-q \in \gzN$ and 
  $p \cdot1 - q \cdot 1 = n \cdot 1 = \sum_{i=1}^n 1 >0$, where the latter inequality follows by induction on $n$. 
  Hence  the map $\Z \to \fldF$, $ p \mapsto p \cdot 1$ is order preserving and in particular injective. 
\end{proof}

\para 
  Since by the preceding result the set of integers is a subset of any ordered field 
  $(\fldF,+,\cdot,0,1,\leq)$, one can ask the question whether $\N$ or $\Z$ are bounded within that 
  field. It will turn out that this is not always so. 

\begin{definition}
  An ordered field $(\fldF,+,\cdot,0,1,\leq)$ is said to be \emph{archimedean ordered} 
  if for every pair of elements $x,y \in \fldF$  with $x > 0$ there 
  exists a natural number $n \in \N$ such that $y < n  \cdot x$. 
\end{definition}

\begin{example}
  The field of rational numbers is archimedean ordered by 
  \Cref{thm:field-rational-numbers-archimedean-ordered}. 
\end{example}

\para An ordered field is in particular an ordered integral domain, so we have an absolute value function
  on an ordered field. 

\begin{definition}
  Let $(\fldF,+,\cdot,0,1,\leq)$ be an ordered field. The \emph{abstract value function} 
  on $\fldF$ is the following map:
  \[
   | \cdot | : \fldF \to \fldF , \quad x \mapsto 
    \begin{cases}
       x & \text{if } x \in \fldF_{>0} = \fldF^+\setminus \{ 0 \} ,\\
       0 &  \text{if } x = 0, \\
       - x  & \text{if } x \in  \fldF_{<0} = \fldF^- \setminus \{ 0 \}  \ . 
    \end{cases}
  \]
  For $x\in \fldF$, $|x|$ is called the \emph{abstract value} of $x$. 
\end{definition}

\begin{remark}
  Since an ordered field is in particular an ordered integral domain, 
  all properties of \Cref{thm:properties-order-ordered-integral-domain}
  and \Cref{thm:properties-absolute-value-ordered-commutative-ring}
  hold true in ordered fields.  
\end{remark}