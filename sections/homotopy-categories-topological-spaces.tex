% Copyright 2016 Markus J. Pflaum, licensed under GNU FDL v1.3
% main author: 
%   Markus J. Pflaum
% contributions by: 
%   Michael Martinez
% 
\section{Homotopy categories of topological spaces}
\label{sec:homotopy-categories-topological-spaces}

\subsec{Notational preliminaries}
\para 
In this chapter, we will always denote by $I$ the compact unit interval 
$[0,1] \subset \R$. Morever, for every topological space $X$ and $t\in I$ we
will denote by $j_{X,t}$ or shortly by $j_t$, if no confusion can arise, the map
$X \rightarrow X\times I$, $x \mapsto (x,t)$. 
Finally, we will denote by $e_{X,t}$ or shortly by
$e_t$ the map  $\shC (I,X) \rightarrow X$, $\gamma \mapsto \gamma (t)$. 

\subsec{Homotopies}
\begin{definition}
Let $X$ and $Y$ be topological spaces. A \emph{homotopy} from $X$ to $Y$ is 
a continuous map $H : X \times I \rightarrow Y$. Given a homotopy $H$ from $X$ to $Y$,
one denotes for every $t \in I$ by $H_t$ the composition $H\circ j_t$
or in other words the map $H_t : X \rightarrow Y$, $x \mapsto H(x,t)$.

With $A$ denoting a subspace of $X$, a continuous map $H : X \times I \rightarrow Y$ is
called a \emph{homotopy from $X$ to $Y$ relative  $A$} if 
$H(a,t)=H(a,0)$ for all $a \in A$ and $t\in I$.

Let $f: X \rightarrow Y$ and $g: X \rightarrow Y$ be maps between
topological spaces $X$ and $Y$. Then one says that $f$  is
\emph{homotopic to $g$} if there is a homotopy $H: X \times I \rightarrow Y$
such that $H_0=f$ and $H_1=g$. This will be denoted by 
$f \simeq g$ or $H: f \simeq g$ and we will often say
that $H$ is a \emph{homotopy between $f$ and $g$} or a \emph{homotopy from $f$ to $g$}.

Let $A \subset X$ be a subspace. Then one says that $f : X \to Y$ is 
\emph{homotopic to $g:X \to Y$ relative  $A$} if there is a homotopy relative 
$A$ between $f$ and $g$ that means a homotopy $H: X \times I \rightarrow Y$
relative  $A$ such that $H_0=f$ and $H_1=g$. 
One denotes this by $f \simeq_A g$, $f \simeq g \text{ rel } A$,
$H: f \simeq_A g$, or $H: f \simeq g \text{ rel } A$.
\end{definition}

\begin{remarks}
\begin{environmentlist}
  \item Observe that homotopy relative  $\emptyset$ is usual homotopy.
  \item If $f,g$ are maps from $X$ to $Y$ such that for some subspace
        $A\subset X$ the relation $f \simeq_A g $ holds true, then  $f|_A=g|_A$.      
\end{environmentlist}
\end{remarks}

%Homotopy over or under spaces.

\begin{proposition}
 Let $X$ and $Y$ be topological spaces and supposed that $A$ is a subspaces of
 $X$. Homotopy relative  $A$ then is an equivalance relation on the
 space $\shC (X,Y)$ of continuous functions from $X$ to $Y$.
 \end{proposition}
\begin{proof}
\begin{claimlist}
\item \textit{The relation $\simeq_A$ is symmetric.} Let $f,g:X
\rightarrow Y$ be continuous maps. If $H: X \times I \rightarrow Y$ is 
a homotopy relative  $A$ from $f$ to $g$, then 
$H^-: X \times I \rightarrow Y$ defined by $H^-(x,t) := H(x,1-t)$ is a homotopy relative  
$A$ from $g$ to $f$. 
\item \textit{The relation $\simeq_A$ is reflexive.} 
Let $f:X \rightarrow Y$ be a continuous map. The map $F: X \times I \rightarrow
Y$ defined by $F(x,t) := f(x)$ is a homotopy relative  $A$ from $f$ to $f$. 
\item \textit{The relation $\simeq_A$ is transitive.} 
Let $f,g,h:X \rightarrow Y$ be continuous maps such that there exist homotopies $F: f \simeq_A g$ and 
$G: g \simeq_A h$. Define $H:X \times I \rightarrow Y$ by 
\begin{equation*}
  H(x,t):=
    \begin{cases} 
       F(x,2t), & \text{if $0 \leq t \leq \frac{1}{2}$}, \\ 
       G(x,2t-1), & \text{if $\frac{1}{2} \leq t \leq 1$.}
    \end{cases}
\end{equation*}
Since $F(x,1)=g (x) =G(x,0)$ for all $x \in X$, the map $H$ is well-defined 
and continuous. Moreover, $H_0=F_0=f$, $H_1=G_1=h$, and
$H(a,t) = H(a,\frac{1}{2}) = g(a)$ for all $a\in A$, so that $H$ is a homotopy
relative  $A$ between $f$ and $h$.   
\end{claimlist}
\end{proof}

\para 
The equivalence class of a continuous map $f:X \rightarrow Y$ with
respect to homotopy relative  $A$ will be denoted $[f]_{\simeq_A}$
respectively by $[f]_\simeq$ or $[f]$ if $A=\emptyset$. 
The set of equivalence classes $[f]_{\simeq_A}$ will be denoted $[X,Y]_A$. 
For ease of notation, one puts $[X,Y]:=[X,Y]_\emptyset$.

\begin{proposition}
Let $f_1, f_2:X \rightarrow Y$ and $g_1,g_2: Y \rightarrow Z$ be continuous. If $F:f_1 \simeq f_2$ and $G: g_1\simeq g_2$, then $g_1 \circ f_1 \simeq g_2 \circ f_2$. In other words, homotopy is a natural equivalance relation on the morphisms of the category of topological spaces.
\end{proposition}

\begin{proof}
Construct $H: g_1 \circ f_1 \simeq g_2 \circ f_2$ by $H(x,t):=G(F(x,t),t)$ for all $x \in X, t \in I$. This function is continuous because $F$ and $G$ are. Furthermore, $$H(x,0)=G(F(x,0),0)=G(f_1(x),0)=g_1(f_1(x))$$ and $$H(x,1)=G(F(x,1),1)=G(f_2(x),1)=g_2(f_2(x))$$ so we have the desired homotopy.
\end{proof}

Now that we have an equivalance relation on the morphisms of $\category{Top}$  which is compatible with composition 
we can define a corresponding quotient category. 

\begin{definition}
 The \emph{homotopy category of topological spaces}, $\category{hTop}$, is the
 category with objects being topological spaces and morphisms being homotopy 
 classes of continuous maps.
\end{definition}

\begin{remark}
  Let $f:X \to Y$ and $g : Y \to Z$ be two continuous maps.  Then we denote the composition 
   of $[g]$ and $[f]$ in $\category{hTop}$ by $[g]\, [f]$, so we 
  usually  omit the symbol $\circ$ for composition in the homotopy category. Note that 
  by definition $ [g]\, [f] = [g\circ f]$.
\end{remark}

\begin{example}
Let $C\in \R^n$ be a convex set, $* \in C$ be a point, and consider the maps 
\[ i:\{*\} \rightarrow C , \: * \mapsto * \quad \text{and} \quad p:C \rightarrow \{*\} , \: x \mapsto * \ . \]
If $C\neq \{*\}$, these maps are not bijective and so are not isomorphisms in $\category{Top}$. However, 
$[p][i]=[p \circ i]=[\id_{\{*\}}]$, and there is a homotopy 
\[ H:C \times I \rightarrow C , \: (x,t) \mapsto *+t(x-*) \]
such that $H_0 = i \circ p$ and $H_1 = \id_C$. Hence $[i]$ and $[p]$ are isomorphisms in the category 
$\category{hTop}$.
\end{example}

\begin{definition}
A continuous function $f:X \rightarrow Y$ is called a \emph{homotopy equivalance} if $[f]$ is an 
isomorphism in $\category{hTop}$ that is if there is a continuous map $g : Y \to X$ such that 
$[f] \, [g] = [\id_Y]$ and $[g] \, [f] = [\id_X]$.  
Two spaces are called homotopy equivalant if there is a homotopy equivalance $f:X \rightarrow Y$.
A space $X$ is called \emph{contractible} if $X$ is homotopy equivalent to a one
point space.
\end{definition}