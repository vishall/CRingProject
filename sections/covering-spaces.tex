% Copyright 2016 Markus J. Pflaum, licensed under GNU FDL v1.3 
% main author: 
%   Markus J. Pflaum 
%
\section{Covering spaces}\label{sec:covering-spaces}

\subsec{Definitions and first properties}

\begin{definition}
  Let $p : \widetilde{X} \to X$ be a continuous map between topological spaces. 
  An open subset $U \subset X$ is called \emph{evenly covered} if 
  the preimage $p^{-1} (U) \subset \widetilde{X}$ is the disjoint union of 
  open subsets $\widetilde{U}_\alpha \subset \widetilde{X}$, $\alpha\in A$,
  such that for each $\alpha \in A$ the restriction 
  $p|_{\widetilde{U}_\alpha} :  \widetilde{U}_\alpha \to U$ is a homeomorphism. 
  
  A surjective continuous map  $p : \widetilde{X} \to X$ is called a 
  \emph{covering} (\emph{of $X$}), a \emph{covering map} or a \emph{covering projection} 
  and $\widetilde{X}$ a \emph{covering space of $X$} if each point of $X$ has an 
  evenly covered open neighborhood. 
  The space $X$ is called the \emph{base space} of the covering. The preimage 
  $p^{-1} (x)$ of a point $x\in X$ is called the \emph{fiber over $x$}, and
  a point $\widetilde{x} \in p^{-1} (x)$ is said to \emph{lie over $x$}. 

  If $p : \widetilde{X} \to X$ and $q: \widetilde{Y} \to Y$ are two coverings,
  a \emph{morphism of coverings} from $p : \widetilde{X} \to X$ to  $q: \widetilde{Y} \to Y$
  is a pair $(\Phi,f)$ of continuous maps $\Phi : \widetilde{X} \to \widetilde{Y}$
  and $f: X \to Y$ such that the diagram 
  \begin{equation}
  \label{diag:morphism-coverings}
  \begin{tikzcd}
    \widetilde{X} \ar[d,"p",swap] \ar[r,"\Phi"] & \widetilde{Y}\ar[d,"q"] \\
    X  \ar[r,"f",swap] & Y
  \end{tikzcd}
  \end{equation}
  commutes. We express that $(\Phi,f)$ is morphism of coverings from 
  $p$ to  $q$ by the notation  $(\Phi,f) : p \to q$. 
  In case $p : \widetilde{X} \to X$ and    $q: \widehat{X} \to X$  
  are two coverings of $X$, a continuous map $\Phi: \widetilde{X} \to \widehat{X}$ 
  is called  a \emph{morphism of coverings of $X$} from $p$ to $q$ if
  $q\circ \Phi = p$. In other words this means that the diagram
  \[
  \begin{tikzcd}
    \widetilde{X} \ar[rr,"\Phi"] \ar[dr,"p"]&&  \widehat{X}\ar[dl,"q",swap] \\
    & X 
  \end{tikzcd}
  \]
  commutes. 
  A homeomorphism $\Phi : \widetilde{X} \to  \widetilde{X}$ 
  which satisfies $p\circ \Phi = p$ is called a \emph{deck transformation} of $p$.

  Let $(X,x_0)$ be a pointed topological space. A morphism 
  $p : (\widetilde{X},\widetilde{x}_0)  \to (X,x_0)$ of pointed topological spaces then is 
  called a  \emph{pointed covering} (\emph{of $(X,x_0)$}) if 
  $p : \widetilde{X}  \to X$ is a  covering map.  

  A \emph{morphism of pointed coverings} from a pointed covering $p : (\widetilde{X},\widetilde{x}_0)  \to (X,x_0)$
  to a pointed covering  $q : (\widetilde{Y},\widetilde{y}_0)  \to (Y,y_0)$ is a pair 
  $(\Phi,f)$ of  morphism of pointed spaces $\Phi : (\widetilde{X},\widetilde{x}_0) \to  (\widetilde{Y},\widetilde{y}_0) $
  and $f: (X,x_0) \to (Y,y_0)$ such that Diag.~\eqref{diag:morphism-coverings} commutes.
  Finally,  if $p :  (\widetilde{X},\widetilde{x}_0)  \to (X,x_0)$ and    $q: (\widehat{X},\widehat{x}_0) \to  (X,x_0)$
  are  two pointed coverings of $(X,x_0)$, then a  morphism of pointed spaces 
  $\Phi : (\widetilde{X},\widetilde{x}_0) \to  (\widehat{X},\widehat{x}_0) $ which satisfies 
  $q\circ \Phi = p$ is called a \emph{morphism of pointed coverings of $(X,x_0)$}.
\end{definition}

\begin{propanddef}
  Covering spaces  as objects together with their morphisms form a category $\category{Cov}$,
  called the \emph{category of covering spaces}. For a topological space $X$ the class of 
  coverings of $X$ as objects together with the morphisms of coverings of $X$ form another 
  category denoted by $\category{Cov}(X)$. It is called the 
  \emph{category of covering spaces of $X$}.
  The automorphisms of $\category{Cov}(X)$ are the deck transformations.

  The category  $\category{Cov}(X)$ can be understood as a subcategory of $\category{Cov}$
  via the functor which is the identical embedding on the object class and which 
  assigns to every morphism 
  $\Phi: \widetilde{X} \to \widehat{X}$ of coverings $p: \widetilde{X} \to X$ and
  $q: \widehat{X} \to X$  of $X$ the morphism $(\Phi,\id_X): p \to q$ in 
  $\category{Cov}$.
  
  Pointed coverings together with their morphisms form a category which is denoted by 
  $\category{Cov}_\bullet$.

  Finally, given a pointed space $(X,x_0)$, the pointed coverings of $(X,x_0)$ together 
  with their morphisms form a category  $\category{Cov}_\bullet(X,x_0)$ which in a 
  canonical way is a subcategory of $\category{Cov}_\bullet$.
\end{propanddef}
 
\begin{proof}
  Clearly, if $p: \widetilde{X} \to X$ is a covering, the pair $(\id_{\widetilde{X}},\id_X)$ is 
  a morphism of covering spaces from $p$ to $p$. If $q: \widetilde{Y} \to Y$
  and $r: \widetilde{Z} \to Z$ are further coverings, and 
  $(\Phi,f): p \to q$ and  $(\Psi,g) : q \to r$ morphisms, then the pair 
  $(\Psi\circ \Phi, g \circ f)$ is a morphism from  $p$ to $r$, since 
  the diagram 
  \[
  \begin{tikzcd}
    \widetilde{X} \ar[d,"p",swap] \ar[r,"\Phi"] & \widetilde{Y}\ar[d,"q"] \ar[r,"\Psi"] 
    & \widetilde{Z}\ar[d,"r"] \\
    X  \ar[r,"f",swap] & Y \ar[r,"g ",swap] & Z
  \end{tikzcd}
  \]
  commutes. Since composition of functions is associative, it now follows that 
  covering spaces together with their morphisms form a category indeed. 
   
  For a covering  $p: \widetilde{X} \to X$, the map $\id_{\widetilde{X}}$ is obviously a 
  morphism of covering spaces of $X$ from $p$ to $p$.
  Moreover, if $\Phi : \widetilde{X} \to  \widehat{X}$ and 
  $\Psi : \widehat{X} \to \widecheck{X} $ are morphisms of covering spaces 
  of $X$ from  $p: \widetilde{X} \to X$ to  $q: \widehat{X} \to X$  and from
  $q: \widehat{X} \to X$ to  $r: \widecheck{X} \to X$, respectively, then the composition 
  $\Psi \circ \Phi$ is a morphism of covering spaces of $X$ from 
  $p$ to $r$, since $r \circ \Psi\circ \Phi = \ q\circ \Phi = p$. So $\category{Cov} (X)$ 
  forms a category indeed.

  The remainder of the claim is obvious. 
\end{proof}

\para 
One of the main goals of this chapter is to show that the category of pointed coverings of a 
pointed topological space $(X,x_0)$ has, under mild assumptions on $X$, an initial object. 
Such an initial object is called a universal cover of $(X,x_0)$.  More precisely: 

\begin{definition}
  Let $(X,x_0)$ be a pointed topolological space.
  A pointed covering  $p:(\widetilde{X},\widetilde{x}_0) \to (X,x_0)$  is called a 
  \emph{universal} (\emph{pointed}) \emph{covering of $(X,x_0)$} 
  if the pointed covering $p:(\widetilde{X},\widetilde{x}_0) \to (X,x_0)$ is an initial object in 
  $\category{Cov}_\bullet (X,x_0)$ which in 
  other words means that it satisfies the following universal property: 
  \begin{axiomlist}
  \item[\textup{(\sffamily UCov)}]  
    For every covering $q : (\widehat{X},\widehat{x}_0) \to (X.x_0)$ there exists a
    unique morphism $\Phi :(\widetilde{X},\widetilde{x}_0) \to  (\widehat{X},\widehat{x}_0) $ of pointed covering spaces  
    from $p$ to $q$. 
  \end{axiomlist}

  A covering  $p:\widetilde{X} \to X$ of a topological space $X$ is said to be a 
  \emph{universal covering of $X$} 
  and $\widetilde{X}$ a \emph{universal covering space} or a \emph{universal cover of $X$}
  if for every $\widetilde{x}_0 \in \widetilde{X}$ the pointed map 
  $p:(\widetilde{X},\widetilde{x}_0) \to (X,x_0)$ with $x_0 := p (\widetilde{x}_0)$ is 
  a universal pointed covering of $(X,x_0)$.   
\end{definition}

\begin{remark}
  We will later see that for reasonable spaces $X$ the condition that 
  $p:(\widetilde{X},\widetilde{x}_0) \to (X,x_0)$ is a universal 
  covering for every $\widetilde{x}_0 \in \widetilde{X}$ and  $x_0 = p (\widetilde{x}_0)$
  holds true already if it is satisfied for one element $\widetilde{x}_0 \in \widetilde{X}$.
\end{remark}

  The crucial property of covering spaces from a homotopy theoretic point of view 
  is that they possess the homotopy lifting property. Before we come to formulate this 
  let us explain what ``lifting'' means.

\begin{definition}
  Let   $p:\widetilde{X} \to X$ be a covering space, and $f: Y \to X$ a continuous map. 
  A continuous map   $\widetilde{f} : Y \to \widetilde{X}$ is then called a \emph{lifting} 
  of $f$ if  the diagram
  \[
  \begin{tikzcd}
    &\widetilde{X}\ar[d,"p"]  \\
    Y \ar[r,"f",swap] \ar[ur,"\widetilde{f}"] & X 
  \end{tikzcd}
  \]
  commutes.
\end{definition}

\begin{theorem}[Homotopy lifting property of covering spaces]
\label{thm:homotopy-lifting-property-covering-spaces}
  Let $p:\widetilde{X} \to X$ be a covering space, $f: Y \to X$ a continuous map, and
  $H : Y \times I \to X$ a homotopy with $H_0 = f$. If $\widetilde{f} : Y \to \widetilde{X}$ 
  is a lifting of $f_0$, then there exists a unique homotopy 
  $\widetilde{H}: Y \times I \to \widetilde{X}$ lifting $H$ 
  such that $\widetilde{H}_0 =\widetilde{f}$.  
\end{theorem}

\begin{corollary}
  Let $\gamma: I \to X$ be a path in the topological space $X$ and $x_0 := \gamma (0)$.
  If $p:\widetilde{X} \to X$ is a covering of $X$ and $\widetilde{x}_0 \in \widetilde{X}$ 
  a point lying over $x_0$, then there exists a unique path 
  $\widetilde{\gamma}: I \to \widetilde{X}$ which lifts $\gamma$ and satisfies 
  $\widetilde{\gamma} (0) = \widetilde{x}_0 $.
\end{corollary}

\subsec{Covering spaces and fiber bundles}

Covering spaces are closely related to fiber bundles. Before we come to stating that
relation, let us recall the definition of  a fiber bundle. 

\begin{definition}
  A quadruple $(E,B,p,F)$ consisting of topological spaces $E$, $B$, and $F$ and
  a continuous map $p:E\to B$ is called a \emph{fiber bundle} if for every point 
  $x \in B$ there exists an open neighborhood $U \subset B$ of $x$ called 
  \emph{trivializing neighborhood} together with a homeomorphism 
  $\varphi: p^{-1} (U) \to U \times F$ such that the diagram
  \[
  \begin{tikzcd}
    p^{-1} (U) \ar[r,"\varphi"] \ar[d,"p",swap] & U \times F \ar[ld,"\pr_1"]\\ 
    U 
  \end{tikzcd}
  \]
  commutes, where $\pr_1 : U \times F \to U$ is projection onto the first factor. 
  The homeomorphism is called a \emph{local trivialization of $E$} (\emph{over $U$}).
  The space $E$ is called the \emph{total space} of the fiber bundle, 
  $B$ the \emph{base}, $F$ the \emph{typical fiber}, and $p:E\to B$ the \emph{projection}. 
\end{definition}

The major ingrediant for proving that covering spaces and fiber bundles with discrete typical 
fibers correspond to each other is the following result. 

\begin{lemma}
  Let $p : \widetilde{X} \to X$ be a continuous map and $U\subset X$ an 
  open subset. Then the following are equivalent:
  \begin{romanlist}
  \item\label{ite:evenly-covered}
    The open set  $U$ is evenly covered.
  \item\label{ite:etale-fiber-bundle-property} 
    There exists a discrete topological space $F$ and a homeomorphism 
    $\varphi: p^{-1} (U) \to U \times F$ such that  
    $p|_{p^{-1} (U)} = \pr_1 \circ \varphi$, where $\pr_1 : U \times F \to U$ denotes
    projection onto the first factor. 
  \end{romanlist}
\end{lemma}
\begin{proof}
  Assume $U$ to be evenly covered, and let $(\widetilde{U}_\alpha)_{\alpha \in A}$ be the 
  family of pairwise disjoint open subsets of $\widetilde{X}$ such that 
  $p^{-1} (U) = \bigcup_{\alpha \in A} \widetilde{U}_\alpha$ and such that 
  $p|_{\widetilde{U}_\alpha} :  \widetilde{U}_\alpha \to U$ is a homeomorphism for every $\alpha \in A$.
  Put $F:=A$ and give $F$ the discrete topology.  Define $\varphi (\widetilde{x})$ for 
  $\widetilde{x} \in p^{-1} (U)$ as the pair $(x,\alpha) \in U \times F$, where $x = p(\widetilde{x})$ 
  and $\alpha$ is the unique element of $F$ such that $\widetilde{x} \in \widetilde{U}_\alpha$. 
  Then  $p|_{p^{-1} (U)} = \pr_1 \circ \varphi$ by construction. Obviously, $\varphi$ is continuous since 
  $\pr_1 \circ \varphi$  is continuous and $\pr_2 \circ \varphi$ locally constant. Moreover, $\varphi$ 
  is invertible with inverse given by $U \times F \to p^{-1} (U)  $, 
  $(x,\alpha) \mapsto \big( p|_{\widetilde{U}_\alpha} \big)^{-1} (x)$. 
  The inverse map is continuous as well, since each of the $p|_{\widetilde{U}_\alpha}$, $\alpha \in A$, is a homeomorphism. 
  This proves \ref{ite:etale-fiber-bundle-property}. 

  Now assume that for the given $U$ there exists a discret topological space $F$ and 
  a homeomorphism  $\varphi: p^{-1} (U) \to U \times F$ such that  
  $p|_{p^{-1} (U)} = \pr_1 \circ \varphi$. Put $A:=F$ and 
  $\widetilde{U}_\alpha := \varphi^{-1} (U \times \{ \alpha \})$ for each $\alpha \in A$. 
  Since $F$ carries the discrete topology, each $\widetilde{U}_\alpha$ is open in 
  $\widetilde{X}$. Moreover, $p^{-1} (U)$ is the disjoint union of the 
  $\widetilde{U}_\alpha$, $\alpha \in A$.  
  The restriction $\varphi|_{\widetilde{U}_\alpha} : \widetilde{U}_\alpha \to U \times \{ \alpha \}$ 
  now is a homeomorphism, and $\pr_1$ is both open and continuous. 
  Hence  $p|_{\widetilde{U}_\alpha} = \pr_1\circ \varphi|_{\widetilde{U}_\alpha}$ is a homeomorphism as 
  well, and \ref{ite:evenly-covered} is proved. 
\end{proof}

\begin{proposition}
  Let $X$ be a connected topological space and 
  $p :\widetilde{X} \to X$ a surjective continuous map. Then the following are equivalent:
  \begin{romanlist}
  \item
     $p :\widetilde{X} \to X$  is a covering. 
  \item
     There exists a discrete topological space $F$ such that $(\widetilde{X}, X, p, F)$ 
     becomes a fiber bundle. 
  \item 
     The map $p$ is a local homeomorphism, and there exists a topological space $F$ such 
     that $(\widetilde{X}, X, p, F)$ becomes a fiber bundle. 
  \end{romanlist}
\end{proposition}


\subsec{Construction of the universal covering}

Not every topological space has a universal cover as the following example shows. 

\begin{example}
  
\end{example}

But for a large class of topological spaces, which in particular contains all smooth manifolds,
one can construct a universal cover.  Before we can formulate this precisely, we need one more 
concept.

\begin{definition}
  A topological space is called \emph{semi-locally simply-connected} if each point 
  $x\in X$ has a neighborhood $N$ such that the group homomorphism 
  $\pi_1 (N,x) \to \pi_1 (X,x)$ is trivial. 
\end{definition}

\begin{example}
  A topological space which is not semi-locally simply connected is the hawaiian  earring. 
\end{example}

\begin{theorem}
  Let $X$ be a connected locally path-connected and semi-locally simply connected topological 
  space. Then $X$  has a universal cover $p:\widetilde{X} \to X$ with 
  $\widetilde{X}$ being locally path-connected and simply-connected. 
\end{theorem}

\begin{proof}
  First fix a point $x_0\in X$ and let $\widetilde{X}$ be the set of all homotopy classes 
  (relative endpoints) of paths in $X$ starting at $x_0$. That means 
  \[
     \widetilde{X} := \big\{ [\gamma] \in \pi_1(X) \mid \gamma (0) = x_0\big \} \ .
  \] 
  The projection $p: \widetilde{X} \to X$ is defined as the map $[\gamma] \mapsto \gamma (1)$. 
  Next we define for every non-empty path-connected open $U \subset X$ and every $[\gamma] \in \widetilde{X}$ with $\gamma (1) \in U$ 
  the set $U_{[\gamma]}$ by 
  \[ 
     U_{[\gamma]} := \big\{ [\gamma * \eta] \in \widetilde{X} \mid \eta \in \shCont (I, U) \: \& \: \eta (0) = \gamma (1) \big\}
  \]
  and let $\base$ be the set of all such $U_{[\gamma]}$. 
  Now we prove several claims. 
  \begin{claimlist}
  \item {\itshape Let $U\subset X$ be open and path-connected. Then for all elements $[\gamma],[\gamma'] \in  \widetilde{X}$ with 
     $p([\gamma]) \in U$ and $ p ([\gamma']) \in U$ the intersection $U_{[\gamma]} \cap U_{[\gamma']}$ is either empty or 
     $U_{[\gamma]}=U_{[\gamma']}$.}\\
     To verify the first claim assume that $[\varrho] \in U_{[\gamma]} \cap U_{[\gamma']}$. Then there exist paths $\mu, \mu' : I \to U$ 
     such that $\mu (0) = \gamma (1)$, $\mu' (0) = \gamma' (1)$, and 
     $[\varrho] = [\gamma * \mu] = [\gamma' * \mu']$.  This implies $\varrho (1) = \mu(1) = \mu'(1)$ and 
     the relation $ [\gamma'] =  [\varrho * (\mu')^{-1}] = [\gamma * \mu * (\mu')^{-1}]$.
     Since $\mu * (\mu')^{-1}$ is a path in $U$ with $\mu * (\mu')^{-1} (0) =  \mu (0) = \gamma (1)$, 
     one concludes that $ [\gamma'] \in  U_{[\gamma]}$. Hence if $\eta' : I \to U$ is a path with $\eta'(0) = \gamma' (1)$, 
     then $ [\gamma' * \eta'] = [\gamma * \mu * (\mu')^{-1} * \eta'] \in  U_{[\gamma]}$ which shows that 
     $U_{[\gamma']} \subset U_{[\gamma]}$. By symmetry, one obtains $U_{[\gamma]} \subset U_{[\gamma']}$, so our 
     first claim is proved. 
  \item 
     {\itshape The set $\base$ of all $U_{}$ as defined above is a basis of a topology on  $\widetilde{X}$.} \\  
     To verify this, let $U_{[\gamma]} , V_{[\varrho]} \in \base$ and assume that $[\mu] \in U_{[\gamma]} \cap V_{[\varrho]}$. 
     Then $\mu (1) \in U\cap V$. 

      
   
  \end{claimlist}

\end{proof}