% Copyright 2016 Markus J. Pflaum, licensed under GNU FDL v1.3
% main author: 
%   Markus J. Pflaum 
%
\section{The real numbers}
\label{sec:real-numbers}
%
\subsec{Complete ordered fields}
%
\para In this section, $(\fldF, +, \cdot , 0, 1, \leq)$ will always denote an ordered field. 

\begin{definition}
  By  a \emph{Dedekind cut} or shortly a \emph{cut} in $\fldF$ one understand a pair $(A,B)$ of subsets 
  $A,B \subset \fldF$ such that the following holds true
  \begin{axiomlist}[DC]
  \item   
    The sets  $A,B $ form a partition of $\fldF$ that means $\fldF = A \cap B$. 
  \item
    The set $A$ does not have a greatest element. 
  \item 
    For all elements $a \in A$ and $b\in B$ one has $a < b$. 
  \end{axiomlist}
  A cut $(A,B)$ of $\fldF$ is called a \emph{gap} if $B$ does not have  a least element. 
\end{definition}


\begin{proposition}
Let $(A,B)$ be a   Dedekind cut in $\fldF$. Then the following holds true:
\begin{romanlist}
\item 
  If $a \in A$ and $ x \in A$ with $x < a$, then $x \in A$.
\item 
  If $b \in B$ and $ x \in B$ with $b < x$, then $x \in B$.
\item 
  The upper part $B$ is always closed in $\fldF$ and satisfies $\closure{\interior{B}}= B$.
\item 
  The lower part $A$ is always open in $\fldF$ and satisfies $\interior{\closure{A}}= A$.
\end{romanlist}
\end{proposition}

\begin{proof}
  
\end{proof}

\begin{proposition}
  For an ordered field $(\fldF, +, \cdot , 0, 1, \leq)$ the following properties 
  are equivalent:
  \begin{romanlist}
   \item\label{ite:least-upper-bound-property}
    Every subset $X \subset \fldF$ bounded above has a supremum. 
  \item\label{ite:nested-interval-property}
    The \emph{nested interval property} holds true in $\fldF$ that means 
  \item\label{ite:cauchy-completeness}
    Every Cauchy sequence in $\fldF$ converges.
  \item
    No Dedekind cut in $\fldF$ is a gap. 
  \end{romanlist}
\end{proposition}

\begin{proof}
  
\end{proof}

%
\subsec{The field of real numbers}
%
\para Let $\Q^\N_\textup{Cauchy}$ denote the set of all Cauchy sequences in $\Q$. Addition and multiplication 
can be extended pointwise from $\Q$ to $\Q^\N_\textup{Cauchy}$ as follows:
\begin{equation*}
\begin{split}
  + : & \: \Q^\N_\textup{Cauchy} \times \Q^\N_\textup{Cauchy} \to \Q^\N_\textup{Cauchy}, \quad 
  \Big( (x_n)_{n\in \N} , (y_n)_{n\in \N} \Big) \mapsto  (x_n+ y_n)_{n\in \N} \\
  \cdot : & \: \Q^\N_\textup{Cauchy} \times \Q^\N_\textup{Cauchy} \to \Q^\N_\textup{Cauchy}, \quad 
  \Big( (x_n)_{n\in \N} , (y_n)_{n\in \N} \Big) \mapsto  (x_n \cdot y_n)_{n\in \N}
\end{split}
\end{equation*}
Addition and multiplication on $\Q^\N_\textup{Cauchy}$ are both associative and commutative since 
these properties hold componentwise. Furthermore, multiplication distributes from the left and from the 
right over addition, again since this property holds componentwise. 
Next define an embedding $\Q \hookrightarrow \Q^\N_\textup{Cauchy}$  by $r \mapsto (r)_{n\in \N}$,
where $(r)_{n\in \N}$ denotes the constant sequence with each component being equal to $r$.
Obviously,  $\Q \hookrightarrow \Q^\N_\textup{Cauchy}$ preserves the operations of addition and multiplication. 
Moreover, the sequence $(0)_{n\in \N}$ serves as neutral element with respect to addition in  $\Q^\N_\textup{Cauchy}$,
and $(1)_{n\in \N}$ as neutral element with respect to multiplication. Hence $\big( \Q^\N_\textup{Cauchy}, + ,\cdot,(0)_{n\in \N}, (1)_{n\in \N} \big)$ 
is a commutative ring and $\Q \hookrightarrow \Q^\N_\textup{Cauchy}$ an injective ring hommorphism. 
Now we define an equivalence relation $\sim$ on $\Q^\N_\textup{Cauchy}$ as follows. Call two Cauchy sequences 
$(x_n)_{n\in \N}$ and $(y_n)_{n\in \N}$ \emph{equivalent}, in signs $(x_n)_{n\in \N} \sim (y_n)_{n\in \N}$, 
if the sequence $(x_n - y_n)_{n\in \N}$ is a null sequence. Denote the equivalence class of a Cauchy sequence $(x_n)_{n\in \N}$ in $\Q$
by $[(x_n)_{n\in \N}]$, the quotient space $\Q^\N_\textup{Cauchy} \, \big/ \!\sim$ by $\R$, and let $q: \Q^\N_\textup{Cauchy} \to \R$ 
be the quotient map which assigns to every element of $\Q^\N_\textup{Cauchy}$ its equivalence class. 
We will now show that $\sim$ is a congruence relation on
$\Q^\N_\textup{Cauchy}$. This means that for Cauchy sequences $(x_n)_{n\in \N} \sim (x_n')_{n\in \N}$ and $(y_n)_{n\in \N} \sim (y_n')_{n\in \N}$
the two added sequences $(x_n + y_n)_{n\in \N}$ and $(x_n'+y')_{n\in \N}$ and the two multiplied sequences 
$(x_n \cdot y_n)_{n\in \N}$ and $(x_n' \cdot y')_{n\in \N}$ are equivalent. 


\begin{lemma}
  The relation $\sim$ is a congruence relation on the commutative ring $\Q^\N_\textup{Cauchy}$. 
\end{lemma}

\begin{proof}
  Asssume that $(x_n)_{n\in \N} \sim (x_n')_{n\in \N}$ and $(y_n)_{n\in \N} \sim (y_n')_{n\in \N}$, where all sequences are
  Cauchy sequences in $\Q$. Then 
  $(x_n - x_n')_{n\in \N}$ and $(y_n - y_n')_{n\in \N}$ are both null sequences, hence there exist for every $\varepsilon >0$  
  natural numbers $N_1 (\varepsilon)$ and $N_2 (\varepsilon) $ such that  
  $\big| x_n - x_n' \big|  < \varepsilon$ for $n \geq N_1 (\varepsilon)  $ and 
  $\big| y_n - y_n' \big|  < \varepsilon$ for $n \geq N_2 (\varepsilon) $.
  Fix $\varepsilon >0$ and put $N := \max \big\{  N_1 (\frac{\varepsilon}{2}) ,N_2 (\frac{\varepsilon}{2}) \big\} $.
  Then one obtains for all $n \geq N $  
  \begin{equation*}
  \begin{split}
     \big| (x_n + y_n) - (x_n' + y_n') \big| \, & = \big| (x_n - x_n') + (y_n - y_n') \big| \leq  
     \big| x_n - x_n' \big| + \big| y_n - y_n' \big| < \frac{\varepsilon}{2} + \frac{\varepsilon}{2} 
     = \varepsilon \ .
  \end{split}
  \end{equation*}
  Now observe that $(x_n')_{n\in \N}$ and $(y_n)_{n\in \N}$ are bounded since both sequences are Cauchy. Choose $M_1,M_2>0$ such that   
  $|x_n'| \leq M_1$ and $|y_n| \leq M_2$ for all $n \in \N$. Now put 
  $N:= \max \big\{  N_1 (\frac{\varepsilon}{2M_2}) ,N_2 (\frac{\varepsilon}{2M_1}) \big\} $. 
  Then one gets for all  $n \geq N $  
  \begin{equation*}
  \begin{split}
     \big| (x_n \cdot y_n) - (x_n' \cdot y_n') \big| \, & = \big| (x_n - x_n') \cdot y_n + x_n' \cdot (y_n - y_n') \big| \leq  \\
     & \leq \big| x_n - x_n' \big| \, | y_n| + \big| y_n - y_n' \big| \, |x_n'|  < 
     \frac{\varepsilon}{2M_2} \, M_2 + \frac{\varepsilon}{2M_1} \, M_1 = \varepsilon \ .
  \end{split}
  \end{equation*}
  Hence  $(x_n + y_n)_{n\in \N} \sim (x_n'+y'_n)_{n\in \N}$ and $(x_n \cdot y_n)_{n\in \N} \sim (x_n' \cdot y'_n)_{n\in \N}$ what had to be shown. 
\end{proof}

As a consequence of the lemma, addition and multiplication on $\Q^\N_\textup{Cauchy}$ descend to unique operations 
$+$ and $\cdot$  on the quotient space $\R$ such that for all rational Cauchy sequences 
$(x_n)_{n\in \N}, (y_n)_{n\in \N}$: 
%\begin{equation}
\begin{align}  
   \label{eq:multiplicativity-quotient-map-cauchy-seqeunces-reals}   
   q \big((x_n)_{n\in \N} + (y_n)_{n\in \N} \big) & = q \big((x_n)_{n\in \N}\big) + q \big((y_n)_{n\in \N}\big)\quad \text{and}\\
   \label{eq:additivity-quotient-map-cauchy-seqeunces-reals}
   q \big((x_n)_{n\in \N} \cdot (y_n)_{n\in \N} \big) & =  q \big((x_n)_{n\in \N}\big) \cdot q \big((y_n)_{n\in \N}\big)
\end{align}
%\end{equation}
The resulting binary operations $+$ and $\cdot$ on $\R$ are both associative and commutative since they are  on $\Q^\N_\textup{Cauchy}$.
Moreover, multiplication on $\R$ distributes over addition, again since $+$ and $\cdot$ have that property on  $\Q^\N_\textup{Cauchy}$.
The mapping $ i : \Q \hookrightarrow \R$, $r \mapsto [(r)_{n\in \N}]$ is injective and preserves addition and multiplication
by Equations \ref{eq:additivity-quotient-map-cauchy-seqeunces-reals} and \ref{eq:multiplicativity-quotient-map-cauchy-seqeunces-reals} 
and since $\Q \to \Q^\N_\textup{Cauchy}$ is a ring homomorphism.
Injectivity follows from the fact that for rational $r, s$ the sequence $(r-s)_{n\in \N}$ is constant, thus a  
null sequence if and only if $r=s$. From now on, we will denote the image of  a rational number $r$ under the mebdding $i$ by $r$ as well. 
In particular the elements $[(0)_{n\in \N}]$ and $[(1)_{n\in \N}]$ will be denoted by $0$ and $1$, respectively. 
Last let us define the set $\R^+$ as the set of all equivalence classes of Cauchy sequences $(x_n)_{n\in \N}$ such that 
$ x_n \in \Q^+ = \{ r \in \Q \mid r \geq 0 \}$ for all $n\in \N$. 

\begin{thmanddef}
  The set $\R$ together with addition $+$, multiplication $\cdot$, and the elements $0,1$ becomes a field.
  Moreover, the set $\R^+$ is a positive cone on $\R$ thus defines a total order $\leq$ on $\R$ respecting the monotony laws by 
  letting $x \leq y$  for $x,y\in \R$ if and only if $y-x \in \R^+$. 
  Altogether, $(\R,+,\cdot,0,1,\leq)$ is a complete ordered field called the \emph{field of real numbers}. Finally,
  $i:\Q \hookrightarrow \R$ an embedding of fields. 
\end{thmanddef}