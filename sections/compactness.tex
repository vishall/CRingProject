% Copyright 2016 Markus J. Pflaum, licensed under GNU FDL v1.3 
% main author: 
%   Markus J. Pflaum 
%
\section{Compactness}\label{sec:compactness}

\subsec{Quasi-compact topological spaces}

\para Before we come to defining quasi-compactness let us recall some relevant notation.
By a \emph{cover} (or \emph{covering}) of a set $X$ one understands a family $\mathcal{U} = ( U_i )_{i\in I}$ of 
subsets $U_i \subset X$ such that $ X  \subset \bigcup_{i\in I} U_i$. 
This terminology also holds for a subset $Y \subset X$. That is a family $\mathcal{U} = ( U_i )_{i\in I}$ of subsets
$U_i \subset X$ is called a \emph{cover} of $Y$ if  $ Y  \subset \bigcup_{i\in I} U_i$.
A \emph{subcover} of a cover  $\mathcal{U} = ( U_i )_{i\in I}$ of $Y$ or shortly a subcover of 
$\mathcal{U}$ then is a subfamily $( U_i )_{i\in J}$ which also covers $Y$ which means that
$J\subset I$ and $Y  \subset \bigcup_{i\in J} U_i$. If $J$ is finite, one calls the subcover $( U_i )_{i\in J}$
a \emph{finite subcover}. If $(X,\topology)$ is a topological space and all elements $U_i$
of a cover $\mathcal{U} = ( U_i )_{i\in I}$ of some $Y \subset X$ are open sets, the cover is called an 
\emph{open cover} of $Y$. 

\begin{proposition}
  Let be a topological spaces $(X,\topology)$. Then the following are equivalent:
  \begin{romanlist}
  \item\label{ite:subcover}
    Every open cover of $X$ has a finite subcover. 
  \item\label{ite:empty-intersection-property} 
    For every family $(A_i)_{i\in I}$ of closed subset $A_i\subset X$ such that $\bigcap_{i\in I} A_i = \emptyset$ 
    there exist finitely many elements $A_{i_1}, \ldots , A_{i_n}$  such that 
    $A_{i_1} \cap \ldots \cap A_{i_n} =\emptyset $.
  \item\label{ite:filter-accummulation-point}
    Every filter on $X$ has an accummulation point.
  \item\label{ite:ultrafilter-convergence}
    Every ultrafilter on $X$ converges.
  \end{romanlist}
\end{proposition}

\begin{proof}
   Assume that \ref{ite:subcover} holds true and let $(A_i)_{i\in I}$ be  a family of closed subset $A_i\subset X$ 
   such that $\bigcap_{i\in I} A_i = \emptyset$. Put $U_i := X \setminus A_i$ for all $i\in I$. 
   Then $(U_i)_{i\in I}$ is an open covering of $X$, hence by assumption there exist $i_1, \ldots , i_n \in I$ 
   such that $X = U_{i_1} \cup \ldots \cup U_{i_n}$. By de Morgan's laws 
   the relation $A_{i_1} \cap \ldots \cap A_{i_n} =\emptyset $ the follows, hence 
   \ref{ite:empty-intersection-property} follows. 
   
   Next assume \ref{ite:empty-intersection-property}, and let $\filter$ be a filter on $X$.
   Then $\closure{A_1} \cap \ldots \cap \closure{A_n} \neq \emptyset $ for all $n\in \N^*$ and 
   $A_1, \ldots , A_n\in \filter$, since $\filter$ is a filter. 
   Hence $\bigcap_{A \in \filter} \closure{A} \neq \emptyset$ by \ref{ite:empty-intersection-property}. 
   Every element of  $\bigcap_{A \in \filter} \closure{A}$ now is an accummulation point of $\filter$, 
   so \ref{ite:filter-accummulation-point} follows.

   By \Cref{thm: },  \ref{ite:filter-accummulation-point} implies \ref{ite:ultrafilter-convergence}.  
   
   Finally assume that every ultrafilter on $X$ converges, and let $\mathcal{U}  = ( U_i )_{i\in I}$ be an 
   open cover of $X$. Assume that $\mathcal{U}$ has no finite subcover.
   For each finite subset $J\subset I$ the set $B_J := X \setminus \bigcup_{i\in J} U_i$ then is non-empty,
   hence $\base := \{ B_J \in \powerset{X} \mid J \subset I \: \&  \: \# J < \infty \}$ is a 
   filter base. Let $\filter$ be an ultrafilter containing $\base$. By assumption $\filter$ converges to some 
   $x\in X$. Since $\mathcal{U}$ is an open covering of $X$ there is some $U_i$ with $x \in U_i$, hence
   $U_i$ since $\filter$ converges to $x$. On the other hand $X \setminus U_i \in \base \subset \filter$ 
   by construction. This is a contradiction, so $\mathcal{U}$ must have  a finite subcover. 
\end{proof}


\begin{definition}[\cite{BouGT}]
  A topological space $(X,\topology)$ is called \emph{quasi-compact}, if every filter on $X$ has an 
  accummulation point.     
\end{definition}

\begin{theorem}[Alexander Subbase Theorem]
  Let $(X,\topology)$ be a topological space, and $\mathscr{S}$ an \emph{adequate} subbase of the topology 
  that is a subbase of $\topology$ such that $X = \bigcup_{S\in \mathscr{S}} S$.
  If every cover of $X$ by elements of $\mathscr{S}$ has a finite subcover, the topological space $(X,\topology)$ 
  is quasi-compact.
\end{theorem}


\subsec{Compact topological spaces}