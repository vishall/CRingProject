% Copyright 2016 Frederic Latremoliere, licensed under GNU FDL v1.3 
% main author: 
%   Frederic Latremoliere
% modifications and contributions by: 
%   Markus J. Pflaum 
%
\section{The category of topological spaces}
\label{sec:category-topological-spaces}

\subsec{Topologies and continuous maps}
\begin{definition}
Let $X$ be a set. By a topology on $X$ on understands  a set  $\topology$ of 
subsets of $X$ such that:
\begin{axiomlist}[Top]
\setcounter{enumi}{-1}
\item\label{axiom:opennness-full-empty-set-open}
      $X\in \topology$ and $\emptyset \in \topology$.
\item\label{axiom:opennness-union-open-sets}
      The union of any collection of elements of $\topology$ is again in $\topology$ that means for each 
      family $(U_i)_{i\in I}$ of $U_i\in \topology$ one has 
      $\bigcup_{i\in I} U_i \in \topology$.
\item \label{axiom:opennness-finite-intersection-open-sets}
      The intersection of finitely many elements of $\topology$ is again in $\topology$ that means 
      for $U_1, \ldots , U_n  \in \topology$ with $n\in \N$ one has 
      $ \bigcap_{i = 1}^n U_i \in \topology$.
\end{axiomlist}

A pair $(X,\topology)$ is a called a \emph{topological space} when $X$ is a 
set and $\topology$ a topology on $X$. Moreover, a subset $U$ of $X$ is 
called \emph{open} if $U\in \topology$ and \emph{closed} if 
$\complement_X U \in \topology$.
\end{definition}

\begin{remark}
  Strictly speaking, Axiom \ref{axiom:opennness-full-empty-set-open} can be derived from 
  Axioms \ref{axiom:opennness-union-open-sets} and \ref{axiom:opennness-finite-intersection-open-sets},
  since the union of an empty family of subsets of $X$ coincides with $\emptyset$, and the 
  intersection of an empty family of subsets of $X$ coincides with $X$. Nevertheless, it is 
  useful to require it, since in proofs one often shows Axiom \ref{axiom:opennness-union-open-sets}
  only for non-empty families of open sets, and 
  Axiom \ref{axiom:opennness-finite-intersection-open-sets} only for the case of the intersection
  of two open subsets. Then it is necessary to verify Axiom \ref{axiom:opennness-full-empty-set-open}
  as well to prove that a given set of subsets of set $X$ is a topology, indeed.  
\end{remark}

\begin{examples}
\label{ex:examples-topological-spaces}
\begin{environmentlist}
\item
  For every set $X$ the power set $\powerset{X}$ is a topology on $X$. It is 
  called the \emph{discrete} or \emph{strongest} topology on $X$.
\item 
  The set $\big\{ \emptyset , X \big\}$  is another topology on a set $X$
  called the   \emph{indiscrete} or \emph{trivial} or 
  \emph{weakest} topology on $X$. Unless $X$ is empty or has only one element,
  the discrete and indiscrete topologies differ.
\item
  Let $S$ be a set $\{ 0, 1\}$.
  Then the set $\big\{  \emptyset , \{ 1 \}, \{ 0, 1 \} \big\} $ is a topology 
  on $S$ which  does neither coincide with the discrete nor the 
  indiscrete topology. The set $S$ with this topology is called 
  \emph{Sierpi\'{n}ski space}. The closed sets of the Sierpi\'{n}ski space
  are $\emptyset$, $\{ 0 \}$ and $S$.
\item\label{ex:standard-topology-reals}
  The euclidean topology $\topology_{\R,\textup{e}}$ 
  on the set $\R$ of real numbers consists of 
  all sets $U\subset \R$ such that for each $x\in U$ there exist real numbers
  $a,b$ satisfying $a < x < b$  and $ \openint{a}{b} \subset U$. 

  Let us show that  $\topology_{\R,\textup{e}}$ is a topology on $\R$ indeed.
  Obviously $\emptyset$ and $\R$ are elements of $\topology_{\R,\textup{e}}$. Let 
  $U,V  \in \topology_{\R,\textup{e}}$ and $x \in U\cap V$. 
  Then there are $a,b,c,d \in \R$ such that 
  $x \in \openint{a}{b}  \subset U$ and $ x\in \openint{c}{d}  \subset V$.
  Put $e := \max \{ a,c\}$ and $f := \min \{ b,d\}$.
  Then $x \in \openint{e}{f}  \subset U \cap V$, which proves 
  $U \cap V \in \topology_{\R,\textup{e}}$. If $\mathscr{U} \subset \topology_{\R,\textup{e}}$
  and $x \in \bigcup \mathscr{U}$, then there exists an $U \in \mathscr{U}$
  with $x \in U$. Choose $a,b \in \R$ such that 
  $x \in \openint{a}{b}  \subset U$. Then 
  $x \in \openint{a}{b} \subset U \subset  \bigcup \mathscr{U}$, which proves
  $\bigcup \mathscr{U} \in \topology_{\R,\textup{e}}$. If not mentioned differently,
  we always assume the  set of real numbers to be equipped with the euclidean topology. 
  One therefore sometimes calls $\topology_{\R,\textup{e}}$  the \emph{standard topology} on
  $\R$. 
\item 
  The euclidean topology  $\topology_{\Q,\textup{e}}$ 
  on the set $\Q$ of rational numbers is defined analogously
  to the previous example as the set of all subset $U\subset \Q$ such that for each 
  $x\in U$ there exist rational numbers $a,b$ with  $a < x < b$  and 
  $ \openint{a}{b} \subset U$. Like for the reals one proves that  $\topology_{\Q,\textup{e}}$ 
  is a topology on $\Q$. Unless mentioned differently it is also always assumed that 
  $\Q$  comes equipped with the euclidean topology. 
\item 
  Let $X$ be a set, and let $\topology_\textup{cof}$ denote the set of all
  subset of $X$ which are either empty or have  finite complement in $X$. 
  Then $\topology_\textup{cof}$ is a topology on $X$ called the 
  \emph{cofinite topology} on $X$. 
\item
  Let $X$ be a (nonempty) set, $(Y,\topology)$ be a topological space, and 
  $f:X\rightarrow Y$ a function. Define
  \[
    f^* \topology := f^{-1} \topology := \{ f^{-1}(U) \mid U \in \topology \} \ . 
  \]
  Then $(X,f^*\topology)$ is a topological space. One calls $f^*\topology$ the 
  \emph{initial topology on $X$ with respect to} $f$ or the \emph{topology induced by} $f$.
  
  Let us verify that $f^*\topology$ is a topology on $X$ indeed. By construction, 
  $f^{-1}(Y)=X$ and $f^{-1}(\emptyset)=\emptyset$, so $\emptyset,X \in f^*\topology$. 
  Now let $(V_i)_{i\in I}$ be a family of elements of $f^*\topology$. 
  In other words we have, for each $i\in I$, $V_i = f^{-1} (U_i)$ for some 
  $U_i\in \topology$. Then $U := \bigcup_{i\in I} U_i \in \topology$ and 
  \[
    \bigcup_{i\in I}V_i =\bigcup_{i\in I}f^{-1} (U_i) = 
    f^{-1} \Big( \bigcup_{i\in I} U_i \Big) = f^{-1} (U) \in f^*\topology \ .
  \]
  Finally, let $V_1,\ldots,V_n \in f^{-1}\topology$. Then, by definition, there exist 
  $U_1, \ldots , U_n \in\topology$ such that $V_i=f^{-1}(U_i)$ for $i=1,\ldots , n$. Thus
  $U :=\bigcap_{i=1}^n U_i \in \topology$ and  
  \[
   \bigcap_{i=1}^n V_i = \bigcap_{i=1}^n f^{-1}(U_i) = 
   f^{-1} \Big( \bigcap_{i=1}^n U_i \Big) = f^{-1} (U) \in f^*\topology \ .
  \]    
\end{environmentlist}
\end{examples}

\Cref{sec:fundamental-examples-topologies} on fundamental 
examples collects several more examples of topologies. 
For now, we will work out a few basic properties 
of topologies and their structure preserving morphisms, the continuous maps 
defined below.

\begin{definition}
Let $(X,\topology_X)$ and $(Y,\topology_Y)$ be two topological spaces
and assume that $f:X\rightarrow Y$ is a function. One says that $f$ is 
\emph{continuous} if for all $U \in \topology_Y$ the preimage $f^{-1}(U)$ 
is open in $X$. The map $f$ is called \emph{open} if $f(V)$ is open in $Y$ 
for all $V \in\topology_X$.  
\end{definition}

\begin{example}
  Any constant function $c : X \to Y$ between two topological spaces is continuous 
  since the preimage of an open set in $Y$ is either the full set $X$ or empty 
  depending on whether the image of $c$ is contained in the open set or not.  
\end{example}

\begin{thmanddef}
\begin{environmentlist}
\item   
The identity map $\id_X$ on a topological space $(X,\topology_X)$ is continuous and open. 
\item
Let $(X,\topology_X)$, $(Y,\topology_Y)$ and $(Z,\topology_Z)$ be three topological 
spaces. Assume that $f:X\rightarrow Y$ and $g:Y\rightarrow Z$ are maps. 
If $f$ and $g$ are both continuous, so is $g\circ f$. If $f$ and $g$ are both open,
then  $g\circ f$ is open as well. 
\item 
Topological spaces as objects together with continuous maps as morphisms form a  category.
It is called the \emph{category of topological spaces} and will be denoted by $\cat{Top}$.  
\end{environmentlist}
\end{thmanddef}

\begin{proof}
It is obvious by definition that the identity map $\id_X$ is continuous and open. 
Now assume that  $f$ and $g$ are continuous and let $U\in\topology_Z$. 
Then $g^{-1}(U)\in\topology_Y$ by continuity of $g$. Hence 
$f^{-1}(g^{-1}(U))\in \topology_X$ by continuity of $f$. So $g\circ f$ is continuous.
If  $f$ and $g$ are open maps, and  $V\in\topology_X$, then 
$f(V) \in \topology_Y$ and $g\circ f (V) = g (f(V)) \in  \topology_Z$. Hence
the composition of two open maps is open, too. The rest of the claim follows immediately. 
\end{proof}


\subsec{Comparison of topologies}

The initial topology $f^* \topology$ induced by a function $f:X\rightarrow Y$ between topological spaces 
is a subset of the topology on $X$ if and only if $f$ is continuous. This motivates the following definition.

\begin{definition}
Let $X$ be a set. Let $\topology_1$ and $\topology_2$ be two topologies on $X$. One says that $\topology_1$ is 
\emph{finer} or \emph{stronger} than $\topology_2$ and $\topology_2$ is \emph{coarser} or \emph{weaker} than $\topology_1$ when 
$\topology_2\subset \topology_1$.
\end{definition}

Of course, inclusion induces an order relation on topologies on a given set. A remarkable property is that any 
nonempty subset of the ordered set of topologies on a given set always admits a greatest lower bound.

\begin{theorem}
\label{thm:topology-generated-set-topologies}
Let $X$ be a set. Let $\mathscr{T}$ be a nonempty set of topologies on $E$. Then the set
\[
  \topology_\mathscr{T} := \bigcap \mathscr{T} = \{ U \in \powerset{X} \mid U \in \topology \text{ for all  }
  \topology \in \mathscr{T} \}
\]
is a topology on $X$ and it is the greatest lower bound of $\mathscr{T}$, where the order between topologies 
is given by inclusion. In other words, $\topology_\mathscr{T}$ is the finest topology contained in each topology from 
$\mathscr{T}$.
\end{theorem}

\begin{proof}
We first show that $\topology_\mathscr{T}$ is a topology. Since each $\topology \in \mathscr{T}$ is a topology on $X$, 
we have $\emptyset,X \in \topology$ for all $\topology \in \mathscr{T}$. Hence $\emptyset,X \in \topology_\mathscr{T}$.

Let $(U_i)_{i\in I}$ be a family of elements $U_i \in \topology_\mathscr{T}$. Let $\topology \in \mathcal{T}$ be arbitrary. By definition 
of $\topology_\mathscr{T}$, we have $U_i \in \topology$ for all $i\in I$. Since $\topology$ is a topology, 
$\bigcup_{i \in I} U_i \in \topology$. Hence, as $\topology$ was arbitrary, $\bigcup_{i\in I} U_i \in \topology_\mathscr{T}$.

Now, let $U_1,\ldots , U_n \in \topology_\mathscr{T}$. Let $\topology \in \mathscr{T}$ be arbitrary. By definition of $\topology_\mathscr{T}$, 
we have $U_1, \ldots , U_n \in \topology$. Therefore, $U_1 \cap \ldots \cap U_n \in \topology$ since $\topology$ is a topology. 
Since $\topology$ was arbitrary in $\mathscr{T}$, we conclude that $U_1 \cap \ldots \cap U_n \in \topology_\mathscr{T}$ by definition.
 
So $\topology_\mathscr{T}$ is a topology on $X$. By construction, $\topology_\mathscr{T} \subset \topology$ for all $\topology \in \mathscr{T}$, 
so $\topology_\mathscr{T}$ is a lower bound for $\mathscr{T}$. Assume given a new topology $\mathcal{Q}$ on $X$ such that 
$\mathcal{Q} \subset \topology$ for all $\topology \in\mathscr{T}$. Let $U \in \mathcal{Q}$. Then we have $U \in \topology$ 
for all $\topology \in \mathscr{T}$. Hence by definition $U\in \topology_\mathscr{T}$. So $\mathcal{Q} \subset \topology_\mathscr{T}$ 
and thus $\topology_\mathscr{T}$ is the greatest lower bound of $\mathscr{T}$.
\end{proof}

\begin{corollary}
Let $X$ be a set and $(Y,\topology_Y)$ be a topological space. The coarsest topology on $X$ which makes a function $f:X\rightarrow Y$ 
continuous is the initial topology $f^*\topology$.
\end{corollary}

\begin{proof}
Let $\mathscr{T}$ be the set of all topologies on $X$ such that $f$ is continuous. By definition, $f^*\topology$ is a lower bound of 
$\mathscr{T}$. Moreover, $f^* \topology \in \mathscr{T}$. Hence $f^*\topology$ is the coarsest topology making the function $f:X\rightarrow Y$ 
continuos.
\end{proof}

We can use \Cref{thm:topology-generated-set-topologies} to define other interesting topologies. Note that trivially $\powerset{X}$ is 
a topology on a given set $X$, so given any $\mathscr{S}\subset \powerset{X}$, there is at least one topology containing $\mathscr{S}$. 
From this:

\begin{propanddef}
Let $X$ be a set, and $\mathscr{S}$ a subset of $\powerset{X}$. The greatest lower bound 
of the set
\[ 
  \mathscr{T} = \{ \topology \in \powerset{\powerset{X}}\mid  \topology
  \textrm{ is a topology on } X \: \& \: \mathscr{S} \subset \topology \} 
\]
is the coarsest topology on $X$ containing $\mathscr{S}$. We call it the 
\emph{topology generated by $\mathscr{S}$ on $X$} and denote it by $\topology_\mathscr{S}$. The topology 
$\topology_\mathscr{S}$ consists of unions of finite intersections of elements of $\mathscr{S}$ that means 
\[ 
  \topology_\mathscr{S} = \Big\{  U \in \powerset{X} \mid \exists J \, \forall j\in J \, \exists n_j\in \N \, 
  \exists U_{j,1},\ldots ,U_{j,n_j} \in \mathscr{S} : \:  U = \bigcup_{j\in J} \bigcap_{k=1}^{n_j} U_{j,k} \Big\} \ .
\]
\end{propanddef}

\begin{proof}
   By definition of $\mathscr{T}$ and \Cref{thm:topology-generated-set-topologies}, $\topology_\mathscr{T} = \bigcap \mathscr{T}$ is a 
   topology on $X$ which contains $\mathscr{S}$. Hence $\topology_\mathscr{T}$ is an element of $\mathscr{T}$ as well
   and a subset of any element of  $\mathscr{T}$. The first claim follows. 
   To verify the second, observe that it suffices to show that 
   \[ 
     \mathcal{Q} := \Big\{  U \in \powerset{X} \mid \exists J \, \forall j\in J \, \exists n_j\in \N \, 
  \exists U_{j,1},\ldots ,U_{j,n_j} \in \mathscr{S} : \:  U = \bigcup_{j\in J} \bigcap_{k=1}^{n_j} U_{j,k} \Big\}
   \]
   is a topology. The set $\mathcal{Q}$ being a topology namely entails $ \topology_\mathscr{S}  \subset \mathcal{Q} $, 
   because $ \mathscr{S} \subset \mathcal{Q}$, 
   and $\mathcal{Q} \subset  \topology_\mathscr{S}$ is clear by definition, since $\topology_\mathscr{S}$ is a topology containing $\mathscr{S}$.
   The second claim $\mathcal{Q} = \topology_\mathscr{S}$ then follows. 
   So let us show that $\mathcal{Q}$ is a topology. Obviously $\emptyset$ and $X$ are elements of $\mathcal{Q}$ 
   because $\bigcup_{i\in \emptyset} U_i = \emptyset$ and $\bigcap_{k=1}^0 U_k = X$. 
   Now assume that $(U_i)_{i\in I}$ is a family of elements of $\mathcal{Q}$. Then there exists for each $i\in I$ a set $J_i$ and
   for every $j\in J_i$ a natural number $n_{i,j}$ together with elements $U_{i,j,1}, \ldots , U_{i,j,n_{i,j}} \in \mathscr{S}$
   such that  
   \[
    U_i = \bigcup_{j\in J_i} \bigcap_{k=1}^{n_{i,j}} U_{i,j,k} \ .
   \]
   Put $J := \bigcup_{i\in I} \{ i \} \times J_i$. Then
   \[
    U :=  \bigcup_{i\in I} U_i = \bigcup_{i\in I} \bigcup_{j\in J_i} \bigcap_{k=1}^{n_{i,j}} U_{i,j,k}
      =  \bigcup_{(i,j) \in J} \bigcap_{k=1}^{n_{i,j}} U_{i,j,k} \in \mathcal{Q} \ .
   \]
   Last assume $U_1, \ldots U_n \in \topology$ where $n\in \N$.  Then one can find for each $i \in \{ 1,\ldots, n \}$ a set $J_i$ and
   for every $j\in J_i$ a natural number $n_{i,j}$ together with elements $U_{i,j,1}, \ldots , U_{i,j,n_{i,j}} \in \mathscr{S}$
   such that  
   \[
    U_i = \bigcup_{j\in J_i} \bigcap_{k=1}^{n_{i,j}} U_{i,j,k} \ .
   \]
   Put $J := J_1 \times \ldots \times J_n$. Then
   \[
     U := \bigcap_{i=1}^n U_i =  \bigcap_{i=1}^n \bigcup_{j\in J_i} \bigcap_{k=1}^{n_{i,j}} U_{i,j,k} = 
     \bigcup_{(j_1,\ldots ,j_n)\in J} \: \bigcap_{k_1=1}^{n_{1,j_1}} U_{1,j_1,k_1} \cap \ldots \cap  \bigcap_{k_n=1}^{n_{n,j_n}} U_{n,j_n,k_n} 
     \in \mathcal{Q} \ .  
   \]     
   Hence $\mathcal{Q}$ is a topology, indeed, and the proposition is proved. 
\end{proof}

\begin{definition}
   Let $X$ be a set, and $\topology$ a topology on $X$. One calls a subset 
   $\mathscr{S} \subset \topology$ a \emph{subbase} or \emph{subbasis} of the 
   topology $\topology$ if $\topology$ coincides with  $\topology_\mathscr{S}$. 
   If in addition $X = \bigcup_{S\in \mathscr{S}} S$, the subbase $\mathscr{S}$
   is said to be \emph{adequate}. 
\end{definition}

\subsec{Bases of topologies}
When inducing a topology from a family $\mathscr{B}$ of subsets of some set $X$, the fact that $\mathscr{B}$ enjoys the following 
property greatly simplifies the description of the topology $\topology_\mathscr{B}$ generated by $\mathscr{B}$.

\begin{definition}
Let $X$ be a set. A (\emph{topological}) \emph{base} on $X$ is a subset $\mathscr{B}$ of the powerset $\powerset{X}$ such that
\begin{axiomlist}[Bas]
\item $X = \bigcup_{B \in \mathscr{B}}B $,
\item For all $B,B' \in \mathscr{B}$ and all $ x \in B \cap B'$ there exists 
      a $B'' \in \mathscr{B}$ such that $x \in B''$ and $B'' \subset B \cap B'$.
\end{axiomlist} 
\end{definition}

The main purpose for this definition stems from the following theorem:

\begin{theorem}
Let $X$ be some set. Let $\mathscr{B}$ be a topological basis on $E$. Then the topology generated by $\mathscr{B}$ coincides
with the set of unions of elements of $\mathscr{B}$ that means 
\[
  \topology_\mathscr{B} = \Big\{ \bigcup_{B \in \mathscr{U}} B \mid \mathscr{U} \subset \mathscr{B} \Big\} \ .
\]
\end{theorem}

\begin{proof}
Denote, for this proof, the set $\{ \bigcup \mathcal{U} : \mathcal{U} \subseteq \mathcal{B} \}$, by $\sigma$, and let us abbreviate $\topology(\mathcal{B})$ by $\topology$. We wish to prove that $\topology = \sigma$.
First, note that $\mathcal{B}\subseteq \sigma$ by construction. By definition, $\mathcal{B} \subseteq \topology$ and since $\topology$ is a topology, it is closed under arbitrary unions. Hence $\sigma \subseteq \topology$.
To prove the converse, it is sufficient to show that $\sigma$ is a topology. As it contains $\mathcal{B}$ and $\topology$ is the smallest such topology, this will provide us with the inverse inclusion.
By definition, $\bigcup \emptyset = \emptyset$ and thus $\emptyset \in \sigma$. By assumption, since $\mathcal{B}$ is a basis, $E = \bigcup \mathcal{B}$ so $E\in \sigma$.
As the union of unions of elements in $\mathcal{B}$ is a union of elements in $\mathcal{B}$, $\sigma$ is closed under abritrary unions.
Now, let $U,V$ be elements of $\mathcal{B}$. If $U\cap V= \emptyset$ then $U\cap V \in \sigma$. Assume that $U$ and $V$ are not disjoints. Then by definition of a basis, for all $x\in U\cap V$ there exists $W_x \in \mathcal{B}$ such that $x\in W_x$ and $W_x \subseteq U\cap V$. So:
$$ U\cap V = \bigcup_{x\in U\cap V} W_x$$ and therefore, by definition, $U\cap V \in \sigma$. We conclude that the intersection of two arbitary elements in $\sigma$ is again in $\sigma$ by using the distributivity of the union with respect to the intersection. 
\end{proof}

\begin{definition}
   We shall say that a base $\mathscr{B}$ on a set $X$ is \emph{a base for a topology $\topology$} on $X$ when the smallest topology 
  containing $\mathscr{B}$ coincides with $\topology$, in other words when $\topology = \topology_\mathscr{B}$.
\end{definition}

The typical usage of the preceding theorem comes from the following result.

\begin{corollary}
Let $\mathscr{B}$ be a topological base for a topology $\topology$ on $X$. A subset $U$ of $X$ is in $\topology$ if and only if for 
any $x \in U$ there exists $B \in \mathscr{B}$ such that $x\in B$ and $B \subset U$.
\end{corollary}

\begin{proof}
We showed that any open set for the topology $\topology$ is a union of elements in $\mathcal{B}$: hence if $x \in U$ for $U \in \topology$ then there exists $B\in\mathcal{B}$ such that $x\in B$ and $B \subseteq U$.
Conversely, if $U$ is some subset of $E$ such that for all $x \in U$ there exists $B_x\in\mathcal{B}$ such that $x\in B_x$ and $B_x \subseteq U$ then $U = \bigcup_{x \in U} B_x$ and thus $U\in \topology$. 
\end{proof}

As a basic application, we show that:

\begin{corollary}
Let $(E,\topology_E)$ and $(F,\topology_F)$ be two topological spaces. Let $\mathcal{B}$ be a basis for the topology $\topology_E$. Let $f:E\rightarrow F$. Then $f$ is continuous on $E$ if and only if:
$$\forall V \in \topology_F \;\;\forall x \in f^{-1}(V) \;\; \exists B \in \mathcal{B} \;\; x\in B \wedge B \subset f^{-1}(V)\textrm{.}$$
\end{corollary}

\begin{corollary}
Let $(E,\topology_E)$ and $(F,\topology_F)$ be two topological spaces. Let $\mathcal{B}$ be a basis for the topology $\topology_F$. Let $f:E\rightarrow F$. Then $f$ is continuous on $E$ if and only if:
$$\forall V \in \mathcal{B} \;\; f^{-1}(V) \in \topology_E \textrm{.} \label{basiscont}$$
\end{corollary}

\begin{proof}
By definition, continuity of $f$ implies \ref{basiscont}. Conversely, assume \ref{basiscont} holds. Let $V\in \topology_F$. Then there exists $\mathcal{U} \subseteq \mathcal{B}$ such that $V=\bigcup \mathcal{U}$. Now by assumption, $f^{-1}(B)\in\topology_E$ for all $B\in\mathcal{U}$ and thus $f^{-1}(V) = \bigcup_{B\in\mathcal{U}} f^{-1}(B) \in \topology_E$ since $\topology_E$ is a topology.
\end{proof}

We leave to the reader to write the statement when both $E$ and $F$ have a basis.
