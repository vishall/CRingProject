\def\cprime{$'$}
\begin{thebibliography}{}

\bibitem[\protect\citeauthoryear{Alexandroff \& Hopf}{Alexandroff \&
  Hopf}{1965}]{AleHopT}
Alexandroff, P. \& Hopf, H. (1965).
\newblock {\em Topologie}.
\newblock Erster Band. Grundbegriffe der mengentheoretischen Topologie,
  Topologie der Komplexe, topologische Invarianzs\"atze und anschliessende
  Begriffsbildungen, Verschlingungen im n-dimensionalen euklidischen Raum,
  stetige Abbildungen von Polyedern. Chelsea Publishing Co., New York.

\bibitem[\protect\citeauthoryear{Bourbaki}{Bourbaki}{1989}]{BouAI}
Bourbaki, N. (1989).
\newblock {\em Algebra I. {C}hapters 1--3}.
\newblock Elements of Mathematics (Berlin). Berlin: Springer-Verlag.
\newblock Translated from the French, Reprint of the 1974 English translation.

\bibitem[\protect\citeauthoryear{Bourbaki}{Bourbaki}{2004}]{BouTS}
Bourbaki, N. (2004).
\newblock {\em Theory of sets}.
\newblock Elements of Mathematics (Berlin). Springer-Verlag, Berlin.
\newblock Reprint of the 1968 English translation [Hermann, Paris].

\bibitem[\protect\citeauthoryear{Brown}{Brown}{2006}]{BroTG}
Brown, R. (2006).
\newblock {\em Topology and groupoids}.
\newblock BookSurge, LLC, Charleston, SC.
\newblock Third edition of {{\i}t Elements of modern topology} [McGraw-Hill,
  New York, 1968], With 1 CD-ROM (Windows, Macintosh and UNIX).

\bibitem[\protect\citeauthoryear{Cartan \& Eilenberg}{Cartan \&
  Eilenberg}{1999}]{CarEilHA}
Cartan, H. \& Eilenberg, S. (1999).
\newblock {\em Homological {Al}gebra}.
\newblock Princeton Landmarks in Mathematics. Princeton University Press,
  Princeton, NJ.
\newblock With an appendix by David A. Buchsbaum, Reprint of the 1956 original.

\bibitem[\protect\citeauthoryear{Dedekind}{Dedekind}{1893}]{DedSSZ}
Dedekind, R. (1893).
\newblock {\em Was {S}ind {U}nd {W}as {S}ollen {D}ie {Z}ahlen?\/} (second ed.).
\newblock Braunschweig: Friedrich Vieweg und Sohn.

\bibitem[\protect\citeauthoryear{Dold}{Dold}{1995}]{DolLAT}
Dold, A. (1995).
\newblock {\em Lectures on {A}lgebraic {T}opology}.
\newblock Springer-Verlag Berlin Heidelberg.
\newblock reprint of the 2nd edition (November 1980), originally published as
  volume 200 in the series: Grundlehren der mathematischen Wissenschaften.

\bibitem[\protect\citeauthoryear{Grothendieck}{Grothendieck}{1957a}]{GroCFTRR}
Grothendieck, A. (1957a).
\newblock Classes de faisceaux et th{\'e}or{\`e}me de {R}iemann-{R}och.
\newblock (Mimeographed).
\newblock Paris.

\bibitem[\protect\citeauthoryear{Grothendieck}{Grothendieck}{1957b}]{GroPAH}
Grothendieck, A. (1957b).
\newblock Sur quelques points d'alg{\`e}bre homologique.
\newblock {\em T{\^o}hoku Math. J. (2)}, {\em 9}, 119--221.

\bibitem[\protect\citeauthoryear{Harary}{Harary}{1969}]{HarGT}
Harary, F. (1969).
\newblock {\em Graph Theory}.
\newblock Reading, MA - Menlo Park, CA - London: Addison-Wesley Publishing Co.

\bibitem[\protect\citeauthoryear{Hatcher}{Hatcher}{2002}]{HatAT}
Hatcher, A. (2002).
\newblock {\em Algebraic {T}opology}.
\newblock Cambridge: Cambridge University Press.
\newblock Available at \url{http://www.math.cornell.edu/~hatcher/AT/AT.pdf}.

\bibitem[\protect\citeauthoryear{Kashiwara \& Schapira}{Kashiwara \&
  Schapira}{2006}]{KasShaCS}
Kashiwara, M. \& Schapira, P. (2006).
\newblock {\em Categories and {S}heaves}, volume 332 of {\em Grundlehren der
  Mathematischen Wissenschaften [Fundamental Principles of Mathematical
  Sciences]}.
\newblock Berlin: Springer-Verlag.

\bibitem[\protect\citeauthoryear{Lang}{Lang}{2002}]{LanA}
Lang, S. (2002).
\newblock {\em Algebra\/} (third ed.)., volume 211 of {\em Graduate Texts in
  Mathematics}.
\newblock New York: Springer-Verlag.

\bibitem[\protect\citeauthoryear{Mac~Lane}{Mac~Lane}{1998}]{MacLanCWM}
Mac~Lane, S. (1998).
\newblock {\em Categories for the {W}orking {M}athematician\/} (2nd ed.).,
  volume~5 of {\em Graduate Texts in Mathematics}.
\newblock New York: Springer-Verlag.

\bibitem[\protect\citeauthoryear{Mendelson}{Mendelson}{2008}]{MenNSFA}
Mendelson, E. (2008).
\newblock {\em Number {S}ystems and the {F}oundations of {A}nalysis}.
\newblock Dover Books on Mathematics. Dover Publications, Inc.
\newblock Reprint of the Academic Press, New York, 1973 edition.

\bibitem[\protect\citeauthoryear{Moschovakis}{Moschovakis}{2006}]{MoschNST}
Moschovakis, Y. (2006).
\newblock {\em Notes on set theory\/} (Second ed.).
\newblock Undergraduate Texts in Mathematics. New York: Springer.

\bibitem[\protect\citeauthoryear{Savage}{Savage}{2006}]{SavFDAQ}
Savage, A. (2006).
\newblock Finite-dimensional algebras and quivers.
\newblock In J.-P. Fran{\c c}oise, G.~L. Naber, \& S.~T. Tsou (Eds.), {\em
  Encyclopedia of Mathematical Physics}, volume~2  (pp.\ 313--320). Oxford:
  Elsevier.

\bibitem[\protect\citeauthoryear{Steen \& Seebach}{Steen \&
  Seebach}{1995}]{SteSeeCT}
Steen, L.~A. \& Seebach, Jr., J.~A. (1995).
\newblock {\em Counterexamples in topology}.
\newblock Dover Publications, Inc., Mineola, NY.
\newblock Reprint of the second (1978) edition.

\bibitem[\protect\citeauthoryear{von Neumann}{von Neumann}{1923}]{vNeuETZ}
von Neumann, J. (1923).
\newblock Zur {E}inf{\"u}hrung der transfiniten {Z}ahlen.
\newblock {\em Acta litterarum ac scientiarum Ragiae Universitatis Hungaricae
  Francisco-Josephinae, Sectio scientiarum mathematicarum}, {\em 1}, 199--208.

\end{thebibliography}
